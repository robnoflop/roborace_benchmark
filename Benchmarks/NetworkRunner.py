import tensorflow as tf
import numpy as np
import time
import tensorflow.contrib.slim.nets as nets
slim = tf.contrib.slim

from tensorflow.contrib.slim.python.slim.nets import resnet_utils

import sys
import gi
import numpy as np, cv
import cv2
import csv
import time
import datetime
#from PIL import Image
gi.require_version('Gst', '1.0')
from gi.repository import Gst, GObject
from PipelineRunner import PipelineRunner


class NetworkRunner(object):
    model = None

    def __init__(self, model_index, number_of_repeats=None, input_width=None, input_height=None, input_depth = 3, batch_size = 1):
        self.model_index = model_index
	if number_of_repeats is None:
            self.number_of_repeats = 1
        else:
            self.number_of_repeats = number_of_repeats

        if input_width is None:
            self.input_width = 224
        else:
            self.input_width = input_width

        if input_height is None:
            self.input_height = 224
        else:
            self.input_height = input_height

        self.input_depth = input_depth
        self.batch_size = batch_size

        tf.reset_default_graph()
        self.inputs = tf.placeholder(tf.float32, shape=(self.batch_size, self.input_height, self.input_width, self.input_depth), name="inputs")

        self.model = self.get_model(self.inputs)

	#config = tf.ConfigProto()
	#config.gpu_options.allow_growth = True
        #self.sess = tf.Session(config=config)
	self.sess = tf.Session()  
        self.sess.run(tf.initialize_all_variables())


    @staticmethod
    def get_model_list():
        MODELS = [
            #0,
            #1,
            #2,
            #3,
            ## 4,
            #5,
            #6,
            #7,
            #8,
            #9,
            #10,
            #11,
            #12,
            #13,
            14,
            #15
	]
        return MODELS

    def get_random_images(self): 
        """generates random image data"""
        return np.random.rand(self.batch_size, 
                              self.input_height,
                              self.input_width,
                              self.input_depth)

    def get_alexnet_out(self, inputs):
        """creates alexnet and retrun output tensor"""
        with slim.arg_scope(nets.alexnet.alexnet_v2_arg_scope()):
            outputs, _ = nets.alexnet.alexnet_v2(inputs, is_training=False, spatial_squeeze=False)
        return outputs

    def get_inception_v1_out(self, inputs):
        """creates inception v1 and retrun output tensor"""
        outputs, _ = nets.inception.inception_v1(inputs, is_training=False, spatial_squeeze=False)
        return outputs

    def get_inception_v2_out(self, inputs):
        """creates inception v2 and retrun output tensor"""
        outputs, _ = nets.inception.inception_v2(inputs, is_training=False, spatial_squeeze=False)
        return outputs

    def get_inception_v3_out(self, inputs):
        """creates inception v3 and retrun output tensor"""
        outputs, _ = nets.inception.inception_v3(inputs, is_training=False, spatial_squeeze=False)
        return outputs

    def get_inception_v4_out(self, inputs):
        """creates inception v4 and retrun output tensor"""
        outputs, _ = nets.inception.inception_v4(inputs)
        return outputs

    def get_resnet_v1_50_out(self, inputs):
        """creates inception v3 and retrun output tensor"""
	with slim.arg_scope(nets.resnet_v1.resnet_arg_scope(False)):
	        outputs, _ = nets.resnet_v1.resnet_v1_50(inputs)
        return outputs

    def get_resnet_v1_101_out(self, inputs):
        """creates inception v3 and retrun output tensor"""
        with slim.arg_scope(nets.resnet_v1.resnet_arg_scope(False)):
		outputs, _ = nets.resnet_v1.resnet_v1_101(inputs)
        return outputs

    def get_resnet_v1_152_out(self, inputs):
        """creates inception v3 and retrun output tensor"""
        with slim.arg_scope(nets.resnet_v1.resnet_arg_scope(False)):
		outputs, _ = nets.resnet_v1.resnet_v1_152(inputs)
        return outputs

    def get_resnet_v1_200_out(self, inputs):
        """creates inception v3 and retrun output tensor"""
        with slim.arg_scope(nets.resnet_v1.resnet_arg_scope(False)):
		outputs, _ = nets.resnet_v1.resnet_v1_200(inputs)
        return outputs

    def get_resnet_v2_50_out(self, inputs):
        """creates inception v3 and retrun output tensor"""
	with slim.arg_scope(nets.resnet_v2.resnet_arg_scope(False)):
        	outputs, _ = nets.resnet_v2.resnet_v2_50(inputs)
        return outputs

    def get_resnet_v2_101_out(self, inputs):
        """creates inception v3 and retrun output tensor"""
        with slim.arg_scope(nets.resnet_v2.resnet_arg_scope(False)):
        	outputs, _ = nets.resnet_v2.resnet_v2_101(inputs)
        return outputs

    def get_resnet_v2_152_out(self, inputs):
        """creates inception v3 and retrun output tensor"""
        with slim.arg_scope(nets.resnet_v2.resnet_arg_scope(False)):
        	outputs, _ = nets.resnet_v2.resnet_v2_152(inputs)
        return outputs

    def get_resnet_v2_200_out(self, inputs):
        """creates inception v3 and retrun output tensor"""
        with slim.arg_scope(nets.resnet_v2.resnet_arg_scope(False)):
        	outputs, _ = nets.resnet_v2.resnet_v2_200(inputs)
        return outputs

    def get_vgg_16_out(self, inputs):
        """creates inception v3 and retrun output tensor"""
        outputs, _ = nets.vgg.vgg_16(inputs, is_training=False, spatial_squeeze=False)
        return outputs

    def get_vgg_19_out(self, inputs):
        """creates inception v3 and retrun output tensor"""
        outputs, _ = nets.vgg.vgg_19(inputs, is_training=False, spatial_squeeze=False)
        return outputs


    def get_model(self, inputs):
        """
            return the model output for index
            index:
            0 = alexnet
            1 = inception v1
            2 = inception v2
            3 = inception v3
            4 = inception v4
            5 = resnet v1 50
            6 = resnet v1 101
            7 = resnet v1 152
            8 = resnet v1 200
            9 = resnet v2 50
            10 = resnet v2 101
            11 = resnet v2 152
            12 = resnet v2 200
            13 = vgg 16
            14 = vgg 19
        """
        index = self.model_index
        out = None
        if index == 0:
            out = self.get_alexnet_out(inputs)
        elif index == 1:
            out = self.get_inception_v1_out(inputs)
        elif index == 2:
            out = self.get_inception_v2_out(inputs)
        elif index == 3:
            out = self.get_inception_v3_out(inputs)
        elif index == 4:
            out = self.get_inception_v4_out(inputs)
        elif index == 5:
            out = self.get_resnet_v1_50_out(inputs)
        elif index == 6:
            out = self.get_resnet_v1_101_out(inputs)
        elif index == 7:
            out = self.get_resnet_v1_152_out(inputs)
        elif index == 8:
            out = self.get_resnet_v1_200_out(inputs)
        elif index == 9:
            out = self.get_resnet_v2_50_out(inputs)
        elif index == 10:
            out = self.get_resnet_v2_101_out(inputs)
        elif index == 11:
            out = self.get_resnet_v2_152_out(inputs)
        elif index == 12:
            out = self.get_resnet_v2_200_out(inputs)
        elif index == 13:
            out = self.get_resnet_v2_50_out(inputs)
        elif index == 14:
            out = self.get_vgg_16_out(inputs)
        elif index == 15:
            out = self.get_vgg_19_out(inputs)
        return out


    def run_model_itterations(self):
        
        return results


    def run_model(self):
        try:
            results = None
            if self.model is not None:
                feed_dict = {
                    self.inputs : self.get_random_images()
                }
                for _ in range(0, self.number_of_repeats):
                    start = time.time()
                    self.sess.run(self.model, feed_dict=feed_dict)
                    end = time.time()
                    if results is None:
                        results = [[start, end]]
                    else:
                        results = np.append(results, [[start, end]], axis=0)
            return results

        except tf.errors.ResourceExhaustedError:
            return None

    def run_model_with_imput(self, image):

        try:
            if self.model is not None:
                feed_dict = {
                    self.inputs : image
                }
                self.sess.run(self.model, feed_dict=feed_dict)

        except tf.errors.ResourceExhaustedError:
            return "error"
