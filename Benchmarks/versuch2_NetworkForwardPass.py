"""none"""
import time
import datetime
import csv
import numpy as np
from NetworkRunner import NetworkRunner

NUMBER_OF_REPEATS = 500

def main():
    """none"""
    global BATCH_SIZE
    global INPUT_HEIGHT
    global INPUT_WIDTH
    global INPUT_DEPTH
    with open('NetworkForwardPass.csv', 'wb') as csvfile:
        logger = csv.writer(csvfile, delimiter=',',
                            quotechar='|', quoting=csv.QUOTE_MINIMAL)
        logger.writerow(['start', 'end', 'model', 'duration'])
	for model_index in NetworkRunner.get_model_list():
    		print "model: " + str(model_index)
		runner = NetworkRunner(model_index, NUMBER_OF_REPEATS)
		results = runner.run_model()
		
		if results is not None:
			for start, end in results:
			    logger.writerow([datetime.datetime.fromtimestamp(start).strftime("%Y-%m-%d/%H:%M:%S.%f"), 
		                        datetime.datetime.fromtimestamp(end).strftime("%Y-%m-%d/%H:%M:%S.%f"),
		                        model_index, 
		                        end-start])
		    

if __name__ == "__main__":
    main()
#syrupy.py -i 0.001 --no-raw-process-log python test.py

#nvidia-smi -q -g 0 -d UTILIZATION -l

#nvidia-smi -lms 50 -q -x -f log.xml

