import sqlite3
import math

def calc_standard_deviation_estimation(data):
    sumData = 0.0
    squareSum = 0.0
    for d in data:
        squareSum += (float(d) * float(d))
        sumData += float(d)

    upper = (len(data) * squareSum) - (sumData *  sumData)
    under = len(data) * (len(data) - 1)
    squareDeviation = upper/under

    if squareDeviation < 0:
        return 0
    else:
        return math.sqrt(upper / under)

def calc_confidence_intervall(data, mean):
    deviation = calc_standard_deviation_estimation(data)
    t_distibution = 1.645
    intervall_distance = ((t_distibution * deviation) / math.sqrt(len(data)))
    under_intervall = mean - intervall_distance
    upper_intervall = mean + intervall_distance
    return under_intervall, upper_intervall


def calc_mode(data):
    maxCount = 0
    mode = 0
    for a in data:
        count = data.count(a)
        if count > maxCount:
            mode = a
            maxCount = count

    return mode


conn = sqlite3.connect('benchmark2.db')
c = conn.cursor()
c2 = conn.cursor()

c2.execute("""DELETE FROM SimpleMode""")
c2.execute("""DELETE FROM SimpleModeConvidenceIntervall""")
conn.commit()


for size in c.execute("""SELECT width, height, endtime FROM log where width != "" GROUP BY width, height, endtime ORDER BY endtime"""):
    logs = c2.execute("""SELECT *
                        FROM log 
                        WHERE width = ? and height = ? and endtime = ?""", (size[0], size[1], size[2])).fetchall()
    print(size)
    start = logs[0][0]
    end = logs[len(logs)- 1][1]
    duations = []
    for log in logs:
        duations.append(round(float(log[3]), 5))

    durationsMode = calc_mode(duations)
    durationsModeIntervall = calc_confidence_intervall(duations, durationsMode)

    pss = c2.execute("""SELECT *
                        FROM ps
                        WHERE strftime(?) < strftime(ps.DATE||'/'||ps.TIME) and 
                        strftime(?) > strftime(ps.DATE||'/'||ps.TIME)""", (start, end)).fetchall()
    GPUs = []
    gpuSpeeds = []
    cpu0Speeds = []
    cpu1Speeds = []
    cpu2Speeds = []
    cpu3Speeds = []
    AOtherms = []
    CPUtherms = []
    GPUtherms = []
    PLLtherms = []
    PMICDies = []
    Tdiode_tegras = []
    Tboard_tegras = []
    thermalfan_est46s = []
    CPUs = []
    MEMs = []
    RSSs = []
    VSIZEs = []

    if(len(pss) > 0):
        for ps in pss:
            GPUs.append(round(float(ps[3]), 5))
            gpuSpeeds.append(round(float(ps[4]), 5))
            cpu0Speeds.append(round(float(ps[5]), 5))
            cpu1Speeds.append(round(float(ps[6]), 5))
            cpu2Speeds.append(round(float(ps[7]), 5))
            cpu3Speeds.append(round(float(ps[8]), 5))
            AOtherms.append(round(float(ps[9]), 5))
            CPUtherms.append(round(float(ps[10]), 5))
            GPUtherms.append(round(float(ps[11]), 5))
            PLLtherms.append(round(float(ps[12]), 5))
            PMICDies.append(round(float(ps[13]), 5))
            Tdiode_tegras.append(round(float(ps[14]), 5))
            Tboard_tegras.append(round(float(ps[15]), 5))
            thermalfan_est46s.append(round(float(ps[16]), 5))
            CPUs.append(round(float(ps[18]), 5))
            MEMs.append(round(float(ps[19]), 5))
            RSSs.append(round(float(ps[20]), 5))
            VSIZEs.append(round(float(ps[21]), 5))


        GPUMode = calc_mode(GPUs)
        GPUModeIntervall = calc_confidence_intervall(GPUs, GPUMode)

        gpuSpeedMode = calc_mode(gpuSpeeds)
        gpuSpeedModeIntervall = calc_confidence_intervall(gpuSpeeds, gpuSpeedMode)

        cpu0SpeedMode = calc_mode(cpu0Speeds)
        cpu0SpeedModeIntervall = calc_confidence_intervall(cpu0Speeds, cpu0SpeedMode)

        cpu1SpeedMode = calc_mode(cpu1Speeds)
        cpu1SpeedModeIntervall = calc_confidence_intervall(cpu1Speeds, cpu1SpeedMode)

        cpu2SpeedMode = calc_mode(cpu2Speeds)
        cpu2SpeedModeIntervall = calc_confidence_intervall(cpu2Speeds, cpu2SpeedMode)

        cpu3SpeedMode = calc_mode(cpu3Speeds)
        cpu3SpeedModeIntervall = calc_confidence_intervall(cpu3Speeds, cpu3SpeedMode)

        AOthermMode = calc_mode(AOtherms)
        AOthermModeIntervall = calc_confidence_intervall(AOtherms, AOthermMode)

        CPUthermMode = calc_mode(CPUtherms)
        CPUthermModeIntervall = calc_confidence_intervall(CPUtherms, CPUthermMode)

        GPUthermMode = calc_mode(GPUtherms)
        GPUthermModeIntervall = calc_confidence_intervall(GPUtherms, GPUthermMode)

        PLLthermMode = calc_mode(PLLtherms)
        PLLthermModeIntervall = calc_confidence_intervall(PLLtherms, PLLthermMode)

        PMICDieMode = calc_mode(PMICDies)
        PMICDieModeIntervall = calc_confidence_intervall(PMICDies, PMICDieMode)

        Tdiode_tegraMode = calc_mode(Tdiode_tegras)
        Tdiode_tegraModeIntervall = calc_confidence_intervall(Tdiode_tegras, Tdiode_tegraMode)

        Tboard_tegraMode = calc_mode(Tboard_tegras)
        Tboard_tegraModeIntervall = calc_confidence_intervall(Tboard_tegras, Tboard_tegraMode)

        thermalfan_est46Mode = calc_mode(thermalfan_est46s)
        thermalfan_est46ModeIntervall = calc_confidence_intervall(thermalfan_est46s, thermalfan_est46Mode)

        CPUMode = calc_mode(CPUs)
        CPUModeIntervall = calc_confidence_intervall(CPUs, CPUMode)

        MEMMode = calc_mode(MEMs)
        MEMModeIntervall = calc_confidence_intervall(MEMs, MEMMode)

        RSSMode = calc_mode(RSSs)
        RSSModeIntervall = calc_confidence_intervall(RSSs, RSSMode)

        VSIZEMode = calc_mode(VSIZEs)
        VSIZEModeIntervall = calc_confidence_intervall(VSIZEs, VSIZEMode)

        c2.execute('insert into SimpleMode values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
                (str(start),
                    str(end),
                    str(size[0]),
                    str(size[1]),
                    str(size[2]),
                    str(durationsMode),
                    str(GPUMode),
                    str(gpuSpeedMode),
                    str(cpu0SpeedMode),
                    str(cpu1SpeedMode),
                    str(cpu2SpeedMode),
                    str(cpu3SpeedMode),
                    str(AOthermMode),
                    str(CPUthermMode),
                    str(GPUthermMode),
                    str(PLLthermMode),
                    str(PMICDieMode),
                    str(Tdiode_tegraMode),
                    str(Tboard_tegraMode),
                    str(thermalfan_est46Mode),
                    str(CPUMode),
                    str(MEMMode),
                    str(RSSMode),
                    str(VSIZEMode))).fetchall()

        c2.execute('insert into SimpleModeConvidenceIntervall values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
                (str(start),
                    str(end),
                    str(size[0]),
                    str(size[1]),
                    str(size[2]),
                    str(durationsModeIntervall[0]),
                    str(durationsModeIntervall[1]),
                    str(GPUModeIntervall[0]),
                    str(GPUModeIntervall[1]),
                    str(gpuSpeedModeIntervall[0]),
                    str(gpuSpeedModeIntervall[1]),
                    str(cpu0SpeedModeIntervall[0]),
                    str(cpu0SpeedModeIntervall[1]),
                    str(cpu1SpeedModeIntervall[0]),
                    str(cpu1SpeedModeIntervall[1]),
                    str(cpu2SpeedModeIntervall[0]),
                    str(cpu2SpeedModeIntervall[1]),
                    str(cpu3SpeedModeIntervall[0]),
                    str(cpu3SpeedModeIntervall[1]),
                    str(AOthermModeIntervall[0]),
                    str(AOthermModeIntervall[1]),
                    str(CPUthermModeIntervall[0]),
                    str(CPUthermModeIntervall[1]),
                    str(GPUthermModeIntervall[0]),
                    str(GPUthermModeIntervall[1]),
                    str(PLLthermModeIntervall[0]),
                    str(PLLthermModeIntervall[1]),
                    str(PMICDieModeIntervall[0]),
                    str(PMICDieModeIntervall[1]),
                    str(Tdiode_tegraModeIntervall[0]),
                    str(Tdiode_tegraModeIntervall[1]),
                    str(Tboard_tegraModeIntervall[0]),
                    str(Tboard_tegraModeIntervall[1]),
                    str(thermalfan_est46ModeIntervall[0]),
                    str(thermalfan_est46ModeIntervall[1]),
                    str(CPUModeIntervall[0]),
                    str(CPUModeIntervall[1]),
                    str(MEMModeIntervall[0]),
                    str(MEMModeIntervall[1]),
                    str(RSSModeIntervall[0]),
                    str(RSSModeIntervall[1]),
                    str(VSIZEModeIntervall[0]),
                    str(VSIZEModeIntervall[1]))).fetchall()

conn.commit()
c2.close()
c.close()
conn.close()


# maxDuration = c2.execute("""SELECT MAX(duration)
#                             FROM merge 
#                             WHERE model = ? AND batchsize = ?""",
#                             (row[0], row[1],)).fetchone()[0]
# minDuration = c2.execute("""SELECT MIN(duration)
#                             FROM merge
#                             WHERE model = ? AND batchsize = ?""",
#                             (row[0], row[1],)).fetchone()[0]

# maxGpu = c2.execute("""SELECT MAX(gpu) FROM merge WHERE model = ? AND batchsize = ?""",
#                     (row[0], row[1],)).fetchone()[0]
# minGpu = c2.execute("""SELECT MIN(gpu) FROM merge WHERE model = ? AND batchsize = ?""",
#                     (row[0], row[1],)).fetchone()[0]

# maxCpu = c2.execute("""SELECT MAX(cpu) FROM merge WHERE model = ? AND batchsize = ?""",
#                     (row[0], row[1],)).fetchone()[0]
# minCpu = c2.execute("""SELECT MIN(cpu) FROM merge WHERE model = ? AND batchsize = ?""",
#                     (row[0], row[1],)).fetchone()[0]


# avgDuration = c2.execute("""SELECT AVG(duration)
#                             FROM (SELECT duration
#                                 FROM merge
#                                 WHERE
#                                     duration < ? AND
#                                     duration > ? AND
#                                     model = ? AND
#                                     batchsize = ?)""",
#                             (maxDuration, minDuration, row[0], row[1],)).fetchone()

# avgGpu = c2.execute("""SELECT AVG(gpu)
#                             FROM (SELECT gpu
#                                 FROM merge
#                                 WHERE
#                                     gpu < ? AND
#                                     gpu > ? AND
#                                     model = ? AND
#                                     batchsize = ?)""",
#                     (maxGpu, minGpu, row[0], row[1],)).fetchone()

# avgCpu = c2.execute("""SELECT AVG(cpu)
#                         FROM merge
#                         WHERE
#                             model = ? AND
#                             batchsize = ?""",
#                     (row[0], row[1],)).fetchone()

# avgMem = c2.execute("""SELECT AVG(mem)
#                     FROM merge
#                     WHERE
#                         model = ? AND
#                         batchsize = ?""",
#                     (row[0], row[1],)).fetchone()

# avgRss = c2.execute("""SELECT AVG(rss)
#                     FROM merge
#                     WHERE
#                         model = ? AND
#                         batchsize = ?""",
#                     (row[0], row[1],)).fetchone()

# avgVsize = c2.execute("""SELECT AVG(vsize)
#                         FROM merge
#                         WHERE
#                             model = ? AND
#                             batchsize = ?""",
#                         (row[0], row[1],)).fetchone()

# data = list(row) + list(avgDuration) + list(avgGpu) + list(avgCpu) + list(avgMem) + list(avgRss) + list(avgVsize)
# print data