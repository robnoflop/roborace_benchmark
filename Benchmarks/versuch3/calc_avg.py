import sqlite3
import math





def calc_standard_deviation_estimation(data):
    sumData = 0.0
    squareSum = 0.0
    for d in data:
        squareSum += (float(d) * float(d))
        sumData += float(d)

    upper = (len(data) * squareSum) - (sumData *  sumData)
    under = len(data) * (len(data) - 1)
    squareDeviation = upper/under

    if squareDeviation < 0:
        return 0
    else:
        return math.sqrt(upper / under)

def calc_confidence_intervall(data, mean):
    deviation = calc_standard_deviation_estimation(data)
    t_distibution = 1.645
    intervall_distance = ((t_distibution * deviation) / math.sqrt(len(data)))
    under_intervall = mean - intervall_distance
    upper_intervall = mean + intervall_distance
    return under_intervall, upper_intervall




def calc_avg(data):
    sum_data = 0
    for d in data:
        sum_data += float(d)
    return sum_data / len(data)


conn = sqlite3.connect('benchmark2.db')
c = conn.cursor()
c2 = conn.cursor()

c2.execute("""DELETE FROM SimpleAvrage""")
c2.execute("""DELETE FROM SimpleAvrageConvidenceIntervall""")
conn.commit()


for size in c.execute("""SELECT width, height, endtime FROM log where width != "" GROUP BY width, height, endtime ORDER BY endtime"""):
    logs = c2.execute("""SELECT *
                        FROM log 
                        WHERE width = ? and height = ? and endtime = ?""", (size[0], size[1], size[2])).fetchall()
    start = logs[0][0]
    end = logs[len(logs)- 1][1]
    duations = []
    for log in logs:
        duations.append(log[3])

    durationsAVG = calc_avg(duations)
    durationsAVGIntervall = calc_confidence_intervall(duations, durationsAVG)

    pss = c2.execute("""SELECT *
                        FROM ps
                        WHERE strftime(?) < strftime(ps.DATE||'/'||ps.TIME) and 
                        strftime(?) > strftime(ps.DATE||'/'||ps.TIME)""", (start, end)).fetchall()
    GPUs = []
    gpuSpeeds = []
    cpu0Speeds = []
    cpu1Speeds = []
    cpu2Speeds = []
    cpu3Speeds = []
    AOtherms = []
    CPUtherms = []
    GPUtherms = []
    PLLtherms = []
    PMICDies = []
    Tdiode_tegras = []
    Tboard_tegras = []
    thermalfan_est46s = []
    CPUs = []
    MEMs = []
    RSSs = []
    VSIZEs = []

    if(len(pss) == 0):
        print(size)

    if(len(pss) > 0):
        for ps in pss:
            GPUs.append(ps[3])
            gpuSpeeds.append(ps[4])
            cpu0Speeds.append(ps[5])
            cpu1Speeds.append(ps[6])
            cpu2Speeds.append(ps[7])
            cpu3Speeds.append(ps[8])
            AOtherms.append(ps[9])
            CPUtherms.append(ps[10])
            GPUtherms.append(ps[11])
            PLLtherms.append(ps[12])
            PMICDies.append(ps[13])
            Tdiode_tegras.append(ps[14])
            Tboard_tegras.append(ps[15])
            thermalfan_est46s.append(ps[16])
            CPUs.append(ps[18])
            MEMs.append(ps[19])
            RSSs.append(ps[20])
            VSIZEs.append(ps[21])

        
        GPUAVG = calc_avg(GPUs)
        GPUAVGIntervall = calc_confidence_intervall(GPUs, GPUAVG)

        gpuSpeedAVG = calc_avg(gpuSpeeds)
        gpuSpeedAVGIntervall = calc_confidence_intervall(gpuSpeeds, gpuSpeedAVG)

        cpu0SpeedAVG = calc_avg(cpu0Speeds)
        cpu0SpeedAVGIntervall = calc_confidence_intervall(cpu0Speeds, cpu0SpeedAVG)

        cpu1SpeedAVG = calc_avg(cpu1Speeds)
        cpu1SpeedAVGIntervall = calc_confidence_intervall(cpu1Speeds, cpu1SpeedAVG)

        cpu2SpeedAVG = calc_avg(cpu2Speeds)
        cpu2SpeedAVGIntervall = calc_confidence_intervall(cpu2Speeds, cpu2SpeedAVG)

        cpu3SpeedAVG = calc_avg(cpu3Speeds)
        cpu3SpeedAVGIntervall = calc_confidence_intervall(cpu3Speeds, cpu3SpeedAVG)

        AOthermAVG = calc_avg(AOtherms)
        AOthermAVGIntervall = calc_confidence_intervall(AOtherms, AOthermAVG)

        CPUthermAVG = calc_avg(CPUtherms)
        CPUthermAVGIntervall = calc_confidence_intervall(CPUtherms, CPUthermAVG)

        GPUthermAVG = calc_avg(GPUtherms)
        GPUthermAVGIntervall = calc_confidence_intervall(GPUtherms, GPUthermAVG)

        PLLthermAVG = calc_avg(PLLtherms)
        PLLthermAVGIntervall = calc_confidence_intervall(PLLtherms, PLLthermAVG)

        PMICDieAVG = calc_avg(PMICDies)
        PMICDieAVGIntervall = calc_confidence_intervall(PMICDies, PMICDieAVG)

        Tdiode_tegraAVG = calc_avg(Tdiode_tegras)
        Tdiode_tegraAVGIntervall = calc_confidence_intervall(Tdiode_tegras, Tdiode_tegraAVG)

        Tboard_tegraAVG = calc_avg(Tboard_tegras)
        Tboard_tegraAVGIntervall = calc_confidence_intervall(Tboard_tegras, Tboard_tegraAVG)

        thermalfan_est46AVG = calc_avg(thermalfan_est46s)
        thermalfan_est46AVGIntervall = calc_confidence_intervall(thermalfan_est46s, thermalfan_est46AVG)

        CPUAVG = calc_avg(CPUs)
        CPUAVGIntervall = calc_confidence_intervall(CPUs, CPUAVG)

        MEMAVG = calc_avg(MEMs)
        MEMAVGIntervall = calc_confidence_intervall(MEMs, MEMAVG)

        RSSAVG = calc_avg(RSSs)
        RSSAVGIntervall = calc_confidence_intervall(RSSs, RSSAVG)

        VSIZEAVG = calc_avg(VSIZEs)
        VSIZEAVGIntervall = calc_confidence_intervall(VSIZEs, VSIZEAVG)

        c2.execute('insert into SimpleAvrage values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
                (str(start),
                    str(end),
                    str(size[0]),
                    str(size[1]),
                    str(size[2]),
                    str(durationsAVG),
                    str(GPUAVG),
                    str(gpuSpeedAVG),
                    str(cpu0SpeedAVG),
                    str(cpu1SpeedAVG),
                    str(cpu2SpeedAVG),
                    str(cpu3SpeedAVG),
                    str(AOthermAVG),
                    str(CPUthermAVG),
                    str(GPUthermAVG),
                    str(PLLthermAVG),
                    str(PMICDieAVG),
                    str(Tdiode_tegraAVG),
                    str(Tboard_tegraAVG),
                    str(thermalfan_est46AVG),
                    str(CPUAVG),
                    str(MEMAVG),
                    str(RSSAVG),
                    str(VSIZEAVG))).fetchall()


        c2.execute('insert into SimpleAvrageConvidenceIntervall values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
                (str(start),
                    str(end),
                    str(size[0]),
                    str(size[1]),
                    str(size[2]),
                    str(durationsAVGIntervall[0]),
                    str(durationsAVGIntervall[1]),
                    str(GPUAVGIntervall[0]),
                    str(GPUAVGIntervall[1]),
                    str(gpuSpeedAVGIntervall[0]),
                    str(gpuSpeedAVGIntervall[1]),
                    str(cpu0SpeedAVGIntervall[0]),
                    str(cpu0SpeedAVGIntervall[1]),
                    str(cpu1SpeedAVGIntervall[0]),
                    str(cpu1SpeedAVGIntervall[1]),
                    str(cpu2SpeedAVGIntervall[0]),
                    str(cpu2SpeedAVGIntervall[1]),
                    str(cpu3SpeedAVGIntervall[0]),
                    str(cpu3SpeedAVGIntervall[1]),
                    str(AOthermAVGIntervall[0]),
                    str(AOthermAVGIntervall[1]),
                    str(CPUthermAVGIntervall[0]),
                    str(CPUthermAVGIntervall[1]),
                    str(GPUthermAVGIntervall[0]),
                    str(GPUthermAVGIntervall[1]),
                    str(PLLthermAVGIntervall[0]),
                    str(PLLthermAVGIntervall[1]),
                    str(PMICDieAVGIntervall[0]),
                    str(PMICDieAVGIntervall[1]),
                    str(Tdiode_tegraAVGIntervall[0]),
                    str(Tdiode_tegraAVGIntervall[1]),
                    str(Tboard_tegraAVGIntervall[0]),
                    str(Tboard_tegraAVGIntervall[1]),
                    str(thermalfan_est46AVGIntervall[0]),
                    str(thermalfan_est46AVGIntervall[1]),
                    str(CPUAVGIntervall[0]),
                    str(CPUAVGIntervall[1]),
                    str(MEMAVGIntervall[0]),
                    str(MEMAVGIntervall[1]),
                    str(RSSAVGIntervall[0]),
                    str(RSSAVGIntervall[1]),
                    str(VSIZEAVGIntervall[0]),
                    str(VSIZEAVGIntervall[1]))).fetchall()

conn.commit()
c2.close()
c.close()
conn.close()


# maxDuration = c2.execute("""SELECT MAX(duration)
#                             FROM merge 
#                             WHERE model = ? AND batchsize = ?""",
#                             (row[0], row[1],)).fetchone()[0]
# minDuration = c2.execute("""SELECT MIN(duration)
#                             FROM merge
#                             WHERE model = ? AND batchsize = ?""",
#                             (row[0], row[1],)).fetchone()[0]

# maxGpu = c2.execute("""SELECT MAX(gpu) FROM merge WHERE model = ? AND batchsize = ?""",
#                     (row[0], row[1],)).fetchone()[0]
# minGpu = c2.execute("""SELECT MIN(gpu) FROM merge WHERE model = ? AND batchsize = ?""",
#                     (row[0], row[1],)).fetchone()[0]

# maxCpu = c2.execute("""SELECT MAX(cpu) FROM merge WHERE model = ? AND batchsize = ?""",
#                     (row[0], row[1],)).fetchone()[0]
# minCpu = c2.execute("""SELECT MIN(cpu) FROM merge WHERE model = ? AND batchsize = ?""",
#                     (row[0], row[1],)).fetchone()[0]


# avgDuration = c2.execute("""SELECT AVG(duration)
#                             FROM (SELECT duration
#                                 FROM merge
#                                 WHERE
#                                     duration < ? AND
#                                     duration > ? AND
#                                     model = ? AND
#                                     batchsize = ?)""",
#                             (maxDuration, minDuration, row[0], row[1],)).fetchone()

# avgGpu = c2.execute("""SELECT AVG(gpu)
#                             FROM (SELECT gpu
#                                 FROM merge
#                                 WHERE
#                                     gpu < ? AND
#                                     gpu > ? AND
#                                     model = ? AND
#                                     batchsize = ?)""",
#                     (maxGpu, minGpu, row[0], row[1],)).fetchone()

# avgCpu = c2.execute("""SELECT AVG(cpu)
#                         FROM merge
#                         WHERE
#                             model = ? AND
#                             batchsize = ?""",
#                     (row[0], row[1],)).fetchone()

# avgMem = c2.execute("""SELECT AVG(mem)
#                     FROM merge
#                     WHERE
#                         model = ? AND
#                         batchsize = ?""",
#                     (row[0], row[1],)).fetchone()

# avgRss = c2.execute("""SELECT AVG(rss)
#                     FROM merge
#                     WHERE
#                         model = ? AND
#                         batchsize = ?""",
#                     (row[0], row[1],)).fetchone()

# avgVsize = c2.execute("""SELECT AVG(vsize)
#                         FROM merge
#                         WHERE
#                             model = ? AND
#                             batchsize = ?""",
#                         (row[0], row[1],)).fetchone()

# data = list(row) + list(avgDuration) + list(avgGpu) + list(avgCpu) + list(avgMem) + list(avgRss) + list(avgVsize)
# print data