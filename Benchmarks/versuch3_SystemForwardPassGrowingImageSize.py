#!/usr/bin/env python
import sys
import gi
import numpy as np, cv
import cv2
import csv
import time
import datetime
#from PIL import Image
gi.require_version('Gst', '1.0')
from gi.repository import Gst, GObject
from PipelineRunner import PipelineRunner
from NetworkRunner import NetworkRunner


#http://www.equasys.de/standardresolution.html
resolutions = [[320,200],
                [320,240],
                [640,480],
                [800,600],
                #[854,480], error
                [1024,768],
                [1152,768],
                [1280,800],
                [1280,854],
                [1280,960],
                [1280,1024],
                [1400,1050],
                [1440,900],
                [1440,960],
                [1600,1200],
                [1680,1050],
                [1920,1200],
                [2048,1536],
                [2560,1600],
                [2560,2048]
                ]


class Tester(object):

    def run(self):
        with open('SystemForwardPass.csv', 'wb') as csvfile:
            self.logger = csv.writer(csvfile, delimiter=',',
                            quotechar='|', quoting=csv.QUOTE_MINIMAL)
            self.logger.writerow(['model_index', 'starttime', 'endtime', 'duration', 'width', 'height'])

            for res in xrange(224,2016,64):
                PIPELINE = """nvcamerasrc num-buffers=100 ! 
                        video/x-raw(memory:NVMM), width=(int)"""+str(res)+""", height=(int)"""+str(res)+""", format=(string)I420 ! 
                        nvvidconv ! 
                        video/x-raw, format=(string)BGRx ! 
                        videoconvert ! 
                        video/x-raw, format=(string)BGR ! 
                        appsink name=app emit-signals=true sync=false"""


                Gst.init(sys.argv)
                GObject.threads_init()
                pipeline = Gst.parse_launch(PIPELINE)
                app = pipeline.get_by_name( 'app' )
		self.duration = 0

                def on_new_buffer(appsink):
		    if self.duration > 1.5 and self.duration < 2:
	                    return Gst.FlowReturn.OK
                    start = time.time()

                    sample = appsink.emit('pull-sample')
                    buf = sample.get_buffer()
                    caps = sample.get_caps()

                    _, map = buf.map(Gst.MapFlags.READ)
                    data = np.fromstring(map.data, dtype=np.uint8)
                    buf.unmap(map)
                    image = np.reshape(data, (1, caps.get_structure(0).get_value('height'), caps.get_structure(0).get_value('width'), 3))

                    self.networkRunner.run_model_with_imput(image)

                    end = time.time()
                    self.logger.writerow([datetime.datetime.fromtimestamp(start).strftime("%Y-%m-%d/%H:%M:%S.%f"), 
                                    datetime.datetime.fromtimestamp(end).strftime("%Y-%m-%d/%H:%M:%S.%f"),
                                    self.model_index, 
                                    end-start,
                                    res,
                                    res])
		    self.duration = end - start

                    return Gst.FlowReturn.OK


                app.connect('new-sample',on_new_buffer )
                for index in NetworkRunner.get_model_list():
                    self.model_index = index
                    self.networkRunner = NetworkRunner(index, number_of_repeats = 1, input_width = res, input_height = res)
                    print "model: " + str(self.model_index)
                    r = PipelineRunner(pipeline)
                    r.run()
		    del r
		    self.networkRunner = None

   

if __name__ == "__main__":
    tester = Tester()
    tester.run()

