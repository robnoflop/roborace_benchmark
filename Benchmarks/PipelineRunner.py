import gi
gi.require_version('Gst', '1.0')
from gi.repository import Gst, GObject

class PipelineRunner(object):
    """Provides an easy way to run a pre-constructed Gstreamer pipeline much
    like gst-launch"""
    def __init__(self, pipeline, stop_pos=None):
        self.mainloop = GObject.MainLoop()
        self.err, self.dbg = None, None

        def on_error(_bus, message):
            print "error"
            self.err, self.dbg = message.parse_error()
            self.mainloop.quit()

        def on_warning(_bus, message):
            print "warning"
            assert message.type == Gst.MessageType.WARNING
            _err, _dbg = message.parse_warning()
            sys.stderr.write("Warning: %s: %s\n%s\n" % (_err, _err.message, _dbg))

        def on_eos(_bus, _message):
            print "eos"
            pipeline.set_state(Gst.State.NULL)
            self.mainloop.quit()

        bus = pipeline.get_bus()
        bus.connect("message::eos", on_eos)
        bus.connect("message::error", on_error)
        bus.connect("message::warning", on_warning)
        bus.add_signal_watch()

        pipeline.set_state(Gst.State.PAUSED)

        if stop_pos is not None:
            pipeline.seek(
                1.0, Gst.Format.TIME, Gst.SeekFlags.SEGMENT |
                Gst.SeekFlags.FLUSH | Gst.SeekFlags.ACCURATE, Gst.SeekType.SET,
                0, Gst.SeekType.SET, stop_pos)

        pipeline.set_state(Gst.State.PLAYING)
        self.pipeline = pipeline

    def run(self):
        self.mainloop.run()
	self.mainloop.quit()
        if self.err is not None:
            raise RuntimeError("Error running pipeline: %s\n\n%s" %
                               (self.err, self.dbg))

    def __del__(self):
        self.pipeline.set_state(Gst.State.NULL)
        self.pipeline.get_state(0)
