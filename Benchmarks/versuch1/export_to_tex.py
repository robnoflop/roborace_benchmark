import sqlite3
from datetime import datetime


#mapping
#0=1
#1=2
#2=3
#3=4
#5=5
#6=6
#7=7
#8=8
#9=9
#10=10
#11=11
#12=12
#13=13
#14=14

def print_tex_plot(data, caption):
    values = ""
    number = 1
    for d in data:
        values += d[2]
        values += "\t"
        values += str(number)
        values += "\t"
        values += str(float(d[2]) - float(d[3]))
        values += "\n"
        number += 1


    plot = """\\begin{figure}
    \\begin{tikzpicture}
    \\begin{axis}[xbar]
    \\addplot[
        every node near coord/.style={inner ysep=5pt},
        error bars/.cd,
            x dir=both,
            x explicit
    ] 
    table [x error=error] {
    x   y           error\n"""
    plot += values
    plot += """};
    \\end{axis}
    \\end{tikzpicture}
    \\caption{"""
    plot += caption
    plot += """}
    \\end{figure}
    \clearpage"""

    print(plot)


def print_tex_plot_gradient(data, caption):
    values = ""
    time = 0.0
    for d in data:
        start = datetime.strptime(d[0], "%Y-%m-%d/%H:%M:%S.%f")
        end = datetime.strptime(d[1], "%Y-%m-%d/%H:%M:%S.%f")
        time += (end - start).total_seconds()
        values += "(" + str(time) + ", " + d[4] + ")"


    plot = """\\begin{figure}
    \\begin{tikzpicture}
    \\begin{axis}
    \\addplot+[const plot ,no markers]
    coordinates {\n"""
    plot += values
    plot +="""\n};
    \\end{axis}
    \\end{tikzpicture}
    \\caption{"""
    plot += caption
    plot += """}
    \\end{figure}
    \clearpage"""

    print(plot)


messung = "speed_1ms"
#messung = "speed_10ms"
#messung = "normal_1ms"
#messung = "normal_10ms"


conn = sqlite3.connect(messung+'/benchmark.db')
c = conn.cursor()
c2 = conn.cursor()



data = c.execute("""SELECT a.width, a.height, a.`AVG(duration)`, b.`median(durationUnder)`, b.`median(durationUpper)`
FROM SimpleAvrage a , SimpleAvrageConvidenceIntervall b
WHERE a.width = b.width AND a.height = b.height ORDER BY (a.width * a.height)""").fetchall()
print_tex_plot(data, "Webcam Test Avarage Duration (" + messung + ")")

data = c.execute("""SELECT a.width, a.height, a.`AVG(GPU)`, b.`median(GPUUnder)`, b.`median(GPUUpper)`
FROM SimpleAvrage a , SimpleAvrageConvidenceIntervall b
WHERE a.width = b.width AND a.height = b.height ORDER BY (a.width * a.height)""").fetchall()
print_tex_plot(data, "Webcam Test Avarage GPU (" + messung + ")")

data = c.execute("""SELECT a.width, a.height, a.`AVG(gpuSpeed)`, b.`median(gpuSpeedUnder)`, b.`median(gpuSpeedUpper)`
FROM SimpleAvrage a , SimpleAvrageConvidenceIntervall b
WHERE a.width = b.width AND a.height = b.height ORDER BY (a.width * a.height)""").fetchall()
print_tex_plot(data, "Webcam Test Avarage gpuSpeed (" + messung + ")")

data = c.execute("""SELECT a.width, a.height, a.`AVG(cpu0Speed)`, b.`median(cpu0SpeedUnder)`, b.`median(cpu0SpeedUpper)`
FROM SimpleAvrage a , SimpleAvrageConvidenceIntervall b
WHERE a.width = b.width AND a.height = b.height ORDER BY (a.width * a.height)""").fetchall()
print_tex_plot(data, "Webcam Test Avarage cpu0Speed (" + messung + ")")

data = c.execute("""SELECT a.width, a.height, a.`AVG(cpu1Speed)`, b.`median(cpu1SpeedUnder)`, b.`median(cpu1SpeedUpper)`
FROM SimpleAvrage a , SimpleAvrageConvidenceIntervall b
WHERE a.width = b.width AND a.height = b.height ORDER BY (a.width * a.height)""").fetchall()
print_tex_plot(data, "Webcam Test Avarage cpu1Speed (" + messung + ")")

data = c.execute("""SELECT a.width, a.height, a.`AVG(cpu2Speed)`, b.`median(cpu2SpeedUnder)`, b.`median(cpu2SpeedUpper)`
FROM SimpleAvrage a , SimpleAvrageConvidenceIntervall b
WHERE a.width = b.width AND a.height = b.height ORDER BY (a.width * a.height)""").fetchall()
print_tex_plot(data, "Webcam Test Avarage cpu2Speed (" + messung + ")")

data = c.execute("""SELECT a.width, a.height, a.`AVG(cpu3Speed)`, b.`median(cpu3SpeedUnder)`, b.`median(cpu3SpeedUpper)`
FROM SimpleAvrage a , SimpleAvrageConvidenceIntervall b
WHERE a.width = b.width AND a.height = b.height ORDER BY (a.width * a.height)""").fetchall()
print_tex_plot(data, "Webcam Test Avarage cpu3Speed (" + messung + ")")

# data = c.execute("""SELECT a.width, a.height, a.`AVG(``AO-therm``)`, b.`median(AO-thermUnder)`, b.`median(AO-thermUpper)`
# FROM SimpleAvrage a , SimpleAvrageConvidenceIntervall b
# WHERE a.width = b.width AND a.height = b.height ORDER BY (a.width * a.height)""").fetchall()
# print_tex_plot(data, "Webcam Test Avarage AO-therm (" + messung + ")")

data = c.execute("""SELECT a.width, a.height, a.`AVG(``CPU-therm``)`, b.`median(CPU-thermUnder)`, b.`median(CPU-thermUpper)`
FROM SimpleAvrage a , SimpleAvrageConvidenceIntervall b
WHERE a.width = b.width AND a.height = b.height ORDER BY (a.width * a.height)""").fetchall()
print_tex_plot(data, "Webcam Test Avarage CPU-therm (" + messung + ")")

data = c.execute("""SELECT a.width, a.height, a.`AVG(``GPU-therm``)`, b.`median(GPU-thermUnder)`, b.`median(GPU-thermUpper)`
FROM SimpleAvrage a , SimpleAvrageConvidenceIntervall b
WHERE a.width = b.width AND a.height = b.height ORDER BY (a.width * a.height)""").fetchall()
print_tex_plot(data, "Webcam Test Avarage GPU-therm (" + messung + ")")

# data = c.execute("""SELECT a.width, a.height, a.`AVG(``PLL-therm``)`, b.`median(PLL-thermUnder)`, b.`median(PLL-thermUpper)`
# FROM SimpleAvrage a , SimpleAvrageConvidenceIntervall b
# WHERE a.width = b.width AND a.height = b.height ORDER BY (a.width * a.height)""").fetchall()
# print_tex_plot(data, "Webcam Test Avarage PLL-therm (" + messung + ")")

# data = c.execute("""SELECT a.width, a.height, a.`AVG(``PMIC-Die``)`, b.`median(PMIC-DieUnder)`, b.`median(PMIC-DieUpper)`
# FROM SimpleAvrage a , SimpleAvrageConvidenceIntervall b
# WHERE a.width = b.width AND a.height = b.height ORDER BY (a.width * a.height)""").fetchall()
# print_tex_plot(data, "Webcam Test Avarage PMIC-Die (" + messung + ")")

# data = c.execute("""SELECT a.width, a.height, a.`AVG(Tdiode_tegra)`, b.`median(Tdiode_tegraUnder)`, b.`median(Tdiode_tegraUpper)`
# FROM SimpleAvrage a , SimpleAvrageConvidenceIntervall b
# WHERE a.width = b.width AND a.height = b.height ORDER BY (a.width * a.height)""").fetchall()
# print_tex_plot(data, "Webcam Test Avarage Tdiode_tegra (" + messung + ")")

# data = c.execute("""SELECT a.width, a.height, a.`AVG(Tboard_tegra)`, b.`median(Tboard_tegraUnder)`, b.`median(Tboard_tegraUpper)`
# FROM SimpleAvrage a , SimpleAvrageConvidenceIntervall b
# WHERE a.width = b.width AND a.height = b.height ORDER BY (a.width * a.height)""").fetchall()
# print_tex_plot(data, "Webcam Test Avarage Tboard_tegra (" + messung + ")")

# data = c.execute("""SELECT a.width, a.height, a.`AVG(``thermal-fan_est.46``)`, b.`median(thermal-fan_est.46Under)`, b.`median(thermal-fan_est.46Upper)`
# FROM SimpleAvrage a , SimpleAvrageConvidenceIntervall b
# WHERE a.width = b.width AND a.height = b.height ORDER BY (a.width * a.height)""").fetchall()
# print_tex_plot(data, "Webcam Test Avarage thermal-fan_est.46 (" + messung + ")")

data = c.execute("""SELECT a.width, a.height, a.`AVG(CPU)`, b.`median(CPUUnder)`, b.`median(CPUUpper)`
FROM SimpleAvrage a , SimpleAvrageConvidenceIntervall b
WHERE a.width = b.width AND a.height = b.height ORDER BY (a.width * a.height)""").fetchall()
print_tex_plot(data, "Webcam Test Avarage CPU (" + messung + ")")

data = c.execute("""SELECT a.width, a.height, a.`AVG(MEM)`, b.`median(MEMUnder)`, b.`median(MEMUpper)`
FROM SimpleAvrage a , SimpleAvrageConvidenceIntervall b
WHERE a.width = b.width AND a.height = b.height ORDER BY (a.width * a.height)""").fetchall()
print_tex_plot(data, "Webcam Test Avarage MEM (" + messung + ")")

data = c.execute("""SELECT a.width, a.height, a.`AVG(RSS)`, b.`median(RSSUnder)`, b.`median(RSSUpper)`
FROM SimpleAvrage a , SimpleAvrageConvidenceIntervall b
WHERE a.width = b.width AND a.height = b.height ORDER BY (a.width * a.height)""").fetchall()
print_tex_plot(data, "Webcam Test Avarage RSS (" + messung + ")")

data = c.execute("""SELECT a.width, a.height, a.`AVG(VSIZE)`, b.`median(VSIZEUnder)`, b.`median(VSIZEUpper)`
FROM SimpleAvrage a , SimpleAvrageConvidenceIntervall b
WHERE a.width = b.width AND a.height = b.height ORDER BY (a.width * a.height)""").fetchall()
print_tex_plot(data, "Webcam Test Avarage VSIZE (" + messung + ")")




data = c.execute("""SELECT a.width, a.height, a.`median(duration)`, b.`median(durationUnder)`, b.`median(durationUpper)`
FROM SimpleMedian a , SimpleMedianConvidenceIntervall b
WHERE a.width = b.width AND a.height = b.height ORDER BY (a.width * a.height)""").fetchall()
print_tex_plot(data, "Webcam Test Median Duration (" + messung + ")")

# data = c.execute("""SELECT a.width, a.height, a.`median(GPU)`, b.`median(GPUUnder)`, b.`median(GPUUpper)`
# FROM SimpleMedian a , SimpleMedianConvidenceIntervall b
# WHERE a.width = b.width AND a.height = b.height ORDER BY (a.width * a.height)""").fetchall()
# print_tex_plot(data, "Webcam Test Median GPU (" + messung + ")")

# data = c.execute("""SELECT a.width, a.height, a.`median(gpuSpeed)`, b.`median(gpuSpeedUnder)`, b.`median(gpuSpeedUpper)`
# FROM SimpleMedian a , SimpleMedianConvidenceIntervall b
# WHERE a.width = b.width AND a.height = b.height ORDER BY (a.width * a.height)""").fetchall()
# print_tex_plot(data, "Webcam Test Median gpuSpeed (" + messung + ")")

# data = c.execute("""SELECT a.width, a.height, a.`median(cpu0Speed)`, b.`median(cpu0SpeedUnder)`, b.`median(cpu0SpeedUpper)`
# FROM SimpleMedian a , SimpleMedianConvidenceIntervall b
# WHERE a.width = b.width AND a.height = b.height ORDER BY (a.width * a.height)""").fetchall()
# print_tex_plot(data, "Webcam Test Median cpu0Speed (" + messung + ")")

# data = c.execute("""SELECT a.width, a.height, a.`median(cpu1Speed)`, b.`median(cpu1SpeedUnder)`, b.`median(cpu1SpeedUpper)`
# FROM SimpleMedian a , SimpleMedianConvidenceIntervall b
# WHERE a.width = b.width AND a.height = b.height ORDER BY (a.width * a.height)""").fetchall()
# print_tex_plot(data, "Webcam Test Median cpu1Speed (" + messung + ")")

# data = c.execute("""SELECT a.width, a.height, a.`median(cpu2Speed)`, b.`median(cpu2SpeedUnder)`, b.`median(cpu2SpeedUpper)`
# FROM SimpleMedian a , SimpleMedianConvidenceIntervall b
# WHERE a.width = b.width AND a.height = b.height ORDER BY (a.width * a.height)""").fetchall()
# print_tex_plot(data, "Webcam Test Median cpu2Speed (" + messung + ")")

# data = c.execute("""SELECT a.width, a.height, a.`median(cpu3Speed)`, b.`median(cpu3SpeedUnder)`, b.`median(cpu3SpeedUpper)`
# FROM SimpleMedian a , SimpleMedianConvidenceIntervall b
# WHERE a.width = b.width AND a.height = b.height ORDER BY (a.width * a.height)""").fetchall()
# print_tex_plot(data, "Webcam Test Median cpu3Speed (" + messung + ")")

# data = c.execute("""SELECT a.width, a.height, a.`median(AO-therm)`, b.`median(AO-thermUnder)`, b.`median(AO-thermUpper)`
# FROM SimpleMedian a , SimpleMedianConvidenceIntervall b
# WHERE a.width = b.width AND a.height = b.height ORDER BY (a.width * a.height)""").fetchall()
# print_tex_plot(data, "Webcam Test Median AO-therm (" + messung + ")")

# data = c.execute("""SELECT a.width, a.height, a.`median(CPU-therm)`, b.`median(CPU-thermUnder)`, b.`median(CPU-thermUpper)`
# FROM SimpleMedian a , SimpleMedianConvidenceIntervall b
# WHERE a.width = b.width AND a.height = b.height ORDER BY (a.width * a.height)""").fetchall()
# print_tex_plot(data, "Webcam Test Median CPU-therm (" + messung + ")")

# data = c.execute("""SELECT a.width, a.height, a.`median(GPU-therm)`, b.`median(GPU-thermUnder)`, b.`median(GPU-thermUpper)`
# FROM SimpleMedian a , SimpleMedianConvidenceIntervall b
# WHERE a.width = b.width AND a.height = b.height ORDER BY (a.width * a.height)""").fetchall()
# print_tex_plot(data, "Webcam Test Median GPU-therm (" + messung + ")")

# data = c.execute("""SELECT a.width, a.height, a.`median(PLL-therm)`, b.`median(PLL-thermUnder)`, b.`median(PLL-thermUpper)`
# FROM SimpleMedian a , SimpleMedianConvidenceIntervall b
# WHERE a.width = b.width AND a.height = b.height ORDER BY (a.width * a.height)""").fetchall()
# print_tex_plot(data, "Webcam Test Median PLL-therm (" + messung + ")")

# data = c.execute("""SELECT a.width, a.height, a.`median(PMIC-Die)`, b.`median(PMIC-DieUnder)`, b.`median(PMIC-DieUpper)`
# FROM SimpleMedian a , SimpleMedianConvidenceIntervall b
# WHERE a.width = b.width AND a.height = b.height ORDER BY (a.width * a.height)""").fetchall()
# print_tex_plot(data, "Webcam Test Median PMIC-Die (" + messung + ")")

# data = c.execute("""SELECT a.width, a.height, a.`median(Tdiode_tegra)`, b.`median(Tdiode_tegraUnder)`, b.`median(Tdiode_tegraUpper)`
# FROM SimpleMedian a , SimpleMedianConvidenceIntervall b
# WHERE a.width = b.width AND a.height = b.height ORDER BY (a.width * a.height)""").fetchall()
# print_tex_plot(data, "Webcam Test Median Tdiode_tegra (" + messung + ")")

# data = c.execute("""SELECT a.width, a.height, a.`median(Tboard_tegra)`, b.`median(Tboard_tegraUnder)`, b.`median(Tboard_tegraUpper)`
# FROM SimpleMedian a , SimpleMedianConvidenceIntervall b
# WHERE a.width = b.width AND a.height = b.height ORDER BY (a.width * a.height)""").fetchall()
# print_tex_plot(data, "Webcam Test Median Tboard_tegra (" + messung + ")")

# data = c.execute("""SELECT a.width, a.height, a.`median(thermal-fan_est.46)`, b.`median(thermal-fan_est.46Under)`, b.`median(thermal-fan_est.46Upper)`
# FROM SimpleMedian a , SimpleMedianConvidenceIntervall b
# WHERE a.width = b.width AND a.height = b.height ORDER BY (a.width * a.height)""").fetchall()
# print_tex_plot(data, "Webcam Test Median thermal-fan_est.46 (" + messung + ")")

# data = c.execute("""SELECT a.width, a.height, a.`median(CPU)`, b.`median(CPUUnder)`, b.`median(CPUUpper)`
# FROM SimpleMedian a , SimpleMedianConvidenceIntervall b
# WHERE a.width = b.width AND a.height = b.height ORDER BY (a.width * a.height)""").fetchall()
# print_tex_plot(data, "Webcam Test Median CPU (" + messung + ")")

# data = c.execute("""SELECT a.width, a.height, a.`median(MEM)`, b.`median(MEMUnder)`, b.`median(MEMUpper)`
# FROM SimpleMedian a , SimpleMedianConvidenceIntervall b
# WHERE a.width = b.width AND a.height = b.height ORDER BY (a.width * a.height)""").fetchall()
# print_tex_plot(data, "Webcam Test Median MEM (" + messung + ")")

# data = c.execute("""SELECT a.width, a.height, a.`median(RSS)`, b.`median(RSSUnder)`, b.`median(RSSUpper)`
# FROM SimpleMedian a , SimpleMedianConvidenceIntervall b
# WHERE a.width = b.width AND a.height = b.height ORDER BY (a.width * a.height)""").fetchall()
# print_tex_plot(data, "Webcam Test Median RSS (" + messung + ")")

# data = c.execute("""SELECT a.width, a.height, a.`median(VSIZE)`, b.`median(VSIZEUnder)`, b.`median(VSIZEUpper)`
# FROM SimpleMedian a , SimpleMedianConvidenceIntervall b
# WHERE a.width = b.width AND a.height = b.height ORDER BY (a.width * a.height)""").fetchall()
# print_tex_plot(data, "Webcam Test Median VSIZE (" + messung + ")")




data = c.execute("""SELECT a.width, a.height, a.`median(duration)`, b.`median(durationUnder)`, b.`median(durationUpper)`
FROM SimpleMode a , SimpleModeConvidenceIntervall b
WHERE a.width = b.width AND a.height = b.height ORDER BY (a.width * a.height)""").fetchall()
print_tex_plot(data, "Webcam Test Mode Duration (" + messung + ")")

# data = c.execute("""SELECT a.width, a.height, a.`median(GPU)`, b.`median(GPUUnder)`, b.`median(GPUUpper)`
# FROM SimpleMode a , SimpleModeConvidenceIntervall b
# WHERE a.width = b.width AND a.height = b.height ORDER BY (a.width * a.height)""").fetchall()
# print_tex_plot(data, "Webcam Test Mode GPU (" + messung + ")")

# data = c.execute("""SELECT a.width, a.height, a.`median(gpuSpeed)`, b.`median(gpuSpeedUnder)`, b.`median(gpuSpeedUpper)`
# FROM SimpleMode a , SimpleModeConvidenceIntervall b
# WHERE a.width = b.width AND a.height = b.height ORDER BY (a.width * a.height)""").fetchall()
# print_tex_plot(data, "Webcam Test Mode gpuSpeed (" + messung + ")")

# data = c.execute("""SELECT a.width, a.height, a.`median(cpu0Speed)`, b.`median(cpu0SpeedUnder)`, b.`median(cpu0SpeedUpper)`
# FROM SimpleMode a , SimpleModeConvidenceIntervall b
# WHERE a.width = b.width AND a.height = b.height ORDER BY (a.width * a.height)""").fetchall()
# print_tex_plot(data, "Webcam Test Mode cpu0Speed (" + messung + ")")

# data = c.execute("""SELECT a.width, a.height, a.`median(cpu1Speed)`, b.`median(cpu1SpeedUnder)`, b.`median(cpu1SpeedUpper)`
# FROM SimpleMode a , SimpleModeConvidenceIntervall b
# WHERE a.width = b.width AND a.height = b.height ORDER BY (a.width * a.height)""").fetchall()
# print_tex_plot(data, "Webcam Test Mode cpu1Speed (" + messung + ")")

# data = c.execute("""SELECT a.width, a.height, a.`median(cpu2Speed)`, b.`median(cpu2SpeedUnder)`, b.`median(cpu2SpeedUpper)`
# FROM SimpleMode a , SimpleModeConvidenceIntervall b
# WHERE a.width = b.width AND a.height = b.height ORDER BY (a.width * a.height)""").fetchall()
# print_tex_plot(data, "Webcam Test Mode cpu2Speed (" + messung + ")")

# data = c.execute("""SELECT a.width, a.height, a.`median(cpu3Speed)`, b.`median(cpu3SpeedUnder)`, b.`median(cpu3SpeedUpper)`
# FROM SimpleMode a , SimpleModeConvidenceIntervall b
# WHERE a.width = b.width AND a.height = b.height ORDER BY (a.width * a.height)""").fetchall()
# print_tex_plot(data, "Webcam Test Mode cpu3Speed (" + messung + ")")

# data = c.execute("""SELECT a.width, a.height, a.`median(AO-therm)`, b.`median(AO-thermUnder)`, b.`median(AO-thermUpper)`
# FROM SimpleMode a , SimpleModeConvidenceIntervall b
# WHERE a.width = b.width AND a.height = b.height ORDER BY (a.width * a.height)""").fetchall()
# print_tex_plot(data, "Webcam Test Mode AO-therm (" + messung + ")")

# data = c.execute("""SELECT a.width, a.height, a.`median(CPU-therm)`, b.`median(CPU-thermUnder)`, b.`median(CPU-thermUpper)`
# FROM SimpleMode a , SimpleModeConvidenceIntervall b
# WHERE a.width = b.width AND a.height = b.height ORDER BY (a.width * a.height)""").fetchall()
# print_tex_plot(data, "Webcam Test Mode CPU-therm (" + messung + ")")

# data = c.execute("""SELECT a.width, a.height, a.`median(GPU-therm)`, b.`median(GPU-thermUnder)`, b.`median(GPU-thermUpper)`
# FROM SimpleMode a , SimpleModeConvidenceIntervall b
# WHERE a.width = b.width AND a.height = b.height ORDER BY (a.width * a.height)""").fetchall()
# print_tex_plot(data, "Webcam Test Mode GPU-therm (" + messung + ")")

# data = c.execute("""SELECT a.width, a.height, a.`median(PLL-therm)`, b.`median(PLL-thermUnder)`, b.`median(PLL-thermUpper)`
# FROM SimpleMode a , SimpleModeConvidenceIntervall b
# WHERE a.width = b.width AND a.height = b.height ORDER BY (a.width * a.height)""").fetchall()
# print_tex_plot(data, "Webcam Test Mode PLL-therm (" + messung + ")")

# data = c.execute("""SELECT a.width, a.height, a.`median(PMIC-Die)`, b.`median(PMIC-DieUnder)`, b.`median(PMIC-DieUpper)`
# FROM SimpleMode a , SimpleModeConvidenceIntervall b
# WHERE a.width = b.width AND a.height = b.height ORDER BY (a.width * a.height)""").fetchall()
# print_tex_plot(data, "Webcam Test Mode PMIC-Die (" + messung + ")")

# data = c.execute("""SELECT a.width, a.height, a.`median(Tdiode_tegra)`, b.`median(Tdiode_tegraUnder)`, b.`median(Tdiode_tegraUpper)`
# FROM SimpleMode a , SimpleModeConvidenceIntervall b
# WHERE a.width = b.width AND a.height = b.height ORDER BY (a.width * a.height)""").fetchall()
# print_tex_plot(data, "Webcam Test Mode Tdiode_tegra (" + messung + ")")

# data = c.execute("""SELECT a.width, a.height, a.`median(Tboard_tegra)`, b.`median(Tboard_tegraUnder)`, b.`median(Tboard_tegraUpper)`
# FROM SimpleMode a , SimpleModeConvidenceIntervall b
# WHERE a.width = b.width AND a.height = b.height ORDER BY (a.width * a.height)""").fetchall()
# print_tex_plot(data, "Webcam Test Mode Tboard_tegra (" + messung + ")")

# data = c.execute("""SELECT a.width, a.height, a.`median(thermal-fan_est.46)`, b.`median(thermal-fan_est.46Under)`, b.`median(thermal-fan_est.46Upper)`
# FROM SimpleMode a , SimpleModeConvidenceIntervall b
# WHERE a.width = b.width AND a.height = b.height ORDER BY (a.width * a.height)""").fetchall()
# print_tex_plot(data, "Webcam Test Mode thermal-fan_est.46 (" + messung + ")")

# data = c.execute("""SELECT a.width, a.height, a.`median(CPU)`, b.`median(CPUUnder)`, b.`median(CPUUpper)`
# FROM SimpleMode a , SimpleModeConvidenceIntervall b
# WHERE a.width = b.width AND a.height = b.height ORDER BY (a.width * a.height)""").fetchall()
# print_tex_plot(data, "Webcam Test Mode CPU (" + messung + ")")

# data = c.execute("""SELECT a.width, a.height, a.`median(MEM)`, b.`median(MEMUnder)`, b.`median(MEMUpper)`
# FROM SimpleMode a , SimpleModeConvidenceIntervall b
# WHERE a.width = b.width AND a.height = b.height ORDER BY (a.width * a.height)""").fetchall()
# print_tex_plot(data, "Webcam Test Mode MEM (" + messung + ")")

# data = c.execute("""SELECT a.width, a.height, a.`median(RSS)`, b.`median(RSSUnder)`, b.`median(RSSUpper)`
# FROM SimpleMode a , SimpleModeConvidenceIntervall b
# WHERE a.width = b.width AND a.height = b.height ORDER BY (a.width * a.height)""").fetchall()
# print_tex_plot(data, "Webcam Test Mode RSS (" + messung + ")")

# data = c.execute("""SELECT a.width, a.height, a.`median(VSIZE)`, b.`median(VSIZEUnder)`, b.`median(VSIZEUpper)`
# FROM SimpleMode a , SimpleModeConvidenceIntervall b
# WHERE a.width = b.width AND a.height = b.height ORDER BY (a.width * a.height)""").fetchall()
# print_tex_plot(data, "Webcam Test Mode VSIZE (" + messung + ")")


# for size in c2.execute("""SELECT width, height
#               FROM Gradients
#               GROUP BY width, height"""):

#     data = c.execute("""SELECT start, end, width, height, `gradient(GPU)`
#                         FROM Gradients
#                         WHERE width = ? AND height = ?""", (size[0], size[1])).fetchall()
#     print_tex_plot_gradient(data, "Webcam Test Gradient GPU (" + messung + ") (" + size[0] + ", " + size[1] + ")")

#     data = c.execute("""SELECT start, end, width, height, `gradient(gpuSpeed)`
#                         FROM Gradients
#                         WHERE width = ? AND height = ?""", (size[0], size[1])).fetchall()
#     print_tex_plot_gradient(data, "Webcam Test Gradient gpuSpeed (" + messung + ") (" + size[0] + ", " + size[1] + ")")

#     data = c.execute("""SELECT start, end, width, height, `gradient(cpu0Speed)`
#                         FROM Gradients
#                         WHERE width = ? AND height = ?""", (size[0], size[1])).fetchall()
#     print_tex_plot_gradient(data, "Webcam Test Gradient cpu0Speed (" + messung + ") (" + size[0] + ", " + size[1] + ")")

#     data = c.execute("""SELECT start, end, width, height, `gradient(cpu1Speed)`
#                         FROM Gradients
#                         WHERE width = ? AND height = ?""", (size[0], size[1])).fetchall()
#     print_tex_plot_gradient(data, "Webcam Test Gradient cpu1Speed (" + messung + ") (" + size[0] + ", " + size[1] + ")")

#     data = c.execute("""SELECT start, end, width, height, `gradient(cpu2Speed)`
#                         FROM Gradients
#                         WHERE width = ? AND height = ?""", (size[0], size[1])).fetchall()
#     print_tex_plot_gradient(data, "Webcam Test Gradient cpu2Speed (" + messung + ") (" + size[0] + ", " + size[1] + ")")

#     data = c.execute("""SELECT start, end, width, height, `gradient(cpu3Speed)`
#                         FROM Gradients
#                         WHERE width = ? AND height = ?""", (size[0], size[1])).fetchall()
#     print_tex_plot_gradient(data, "Webcam Test Gradient cpu3Speed (" + messung + ") (" + size[0] + ", " + size[1] + ")")

#     data = c.execute("""SELECT start, end, width, height, `gradient(AO-therm)`
#                         FROM Gradients
#                         WHERE width = ? AND height = ?""", (size[0], size[1])).fetchall()
#     print_tex_plot_gradient(data, "Webcam Test Gradient AO-therm (" + messung + ") (" + size[0] + ", " + size[1] + ")")

#     data = c.execute("""SELECT start, end, width, height, `gradient(CPU-therm)`
#                         FROM Gradients
#                         WHERE width = ? AND height = ?""", (size[0], size[1])).fetchall()
#     print_tex_plot_gradient(data, "Webcam Test Gradient CPU-therm (" + messung + ") (" + size[0] + ", " + size[1] + ")")

#     data = c.execute("""SELECT start, end, width, height, `gradient(GPU-therm)`
#                         FROM Gradients
#                         WHERE width = ? AND height = ?""", (size[0], size[1])).fetchall()
#     print_tex_plot_gradient(data, "Webcam Test Gradient GPU-therm (" + messung + ") (" + size[0] + ", " + size[1] + ")")

#     data = c.execute("""SELECT start, end, width, height, `gradient(PLL-therm)`
#                         FROM Gradients
#                         WHERE width = ? AND height = ?""", (size[0], size[1])).fetchall()
#     print_tex_plot_gradient(data, "Webcam Test Gradient PLL-therm (" + messung + ") (" + size[0] + ", " + size[1] + ")")

#     data = c.execute("""SELECT start, end, width, height, `gradient(PMIC-Die)`
#                         FROM Gradients
#                         WHERE width = ? AND height = ?""", (size[0], size[1])).fetchall()
#     print_tex_plot_gradient(data, "Webcam Test Gradient PMIC-Die (" + messung + ") (" + size[0] + ", " + size[1] + ")")

#     data = c.execute("""SELECT start, end, width, height, `gradient(Tdiode_tegra)`
#                         FROM Gradients
#                         WHERE width = ? AND height = ?""", (size[0], size[1])).fetchall()
#     print_tex_plot_gradient(data, "Webcam Test Gradient Tdiode_tegra (" + messung + ") (" + size[0] + ", " + size[1] + ")")

#     data = c.execute("""SELECT start, end, width, height, `gradient(Tboard_tegra)`
#                         FROM Gradients
#                         WHERE width = ? AND height = ?""", (size[0], size[1])).fetchall()
#     print_tex_plot_gradient(data, "Webcam Test Gradient Tboard_tegra (" + messung + ") (" + size[0] + ", " + size[1] + ")")

#     data = c.execute("""SELECT start, end, width, height, `gradient(thermal-fan_est.46)`
#                         FROM Gradients
#                         WHERE width = ? AND height = ?""", (size[0], size[1])).fetchall()
#     print_tex_plot_gradient(data, "Webcam Test Gradient thermal-fan_est.46 (" + messung + ") (" + size[0] + ", " + size[1] + ")")

#     data = c.execute("""SELECT start, end, width, height, `gradient(CPU)`
#                         FROM Gradients
#                         WHERE width = ? AND height = ?""", (size[0], size[1])).fetchall()
#     print_tex_plot_gradient(data, "Webcam Test Gradient CPU (" + messung + ") (" + size[0] + ", " + size[1] + ")")

#     data = c.execute("""SELECT start, end, width, height, `gradient(MEM)`
#                         FROM Gradients
#                         WHERE width = ? AND height = ?""", (size[0], size[1])).fetchall()
#     print_tex_plot_gradient(data, "Webcam Test Gradient MEM (" + messung + ") (" + size[0] + ", " + size[1] + ")")

#     data = c.execute("""SELECT start, end, width, height, `gradient(RSS)`
#                         FROM Gradients
#                         WHERE width = ? AND height = ?""", (size[0], size[1])).fetchall()
#     print_tex_plot_gradient(data, "Webcam Test Gradient RSS (" + messung + ") (" + size[0] + ", " + size[1] + ")")

#     data = c.execute("""SELECT start, end, width, height, `gradient(VSIZE)`
#                         FROM Gradients
#                         WHERE width = ? AND height = ?""", (size[0], size[1])).fetchall()
#     print_tex_plot_gradient(data, "Webcam Test Gradient VSIZE (" + messung + ") (" + size[0] + ", " + size[1] + ")")
