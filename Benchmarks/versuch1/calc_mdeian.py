import sqlite3
import math





def calc_standard_deviation_estimation(data):
    sumData = 0.0
    squareSum = 0.0
    for d in data:
        squareSum += (float(d) * float(d))
        sumData += float(d)

    upper = (len(data) * squareSum) - (sumData *  sumData)
    under = len(data) * (len(data) - 1)
    squareDeviation = upper/under

    if squareDeviation < 0:
        return 0
    else:
        return math.sqrt(upper / under)

def calc_confidence_intervall(data, mean):
    deviation = calc_standard_deviation_estimation(data)
    t_distibution = 1.645
    intervall_distance = ((t_distibution * deviation) / math.sqrt(len(data)))
    under_intervall = mean - intervall_distance
    upper_intervall = mean + intervall_distance
    return under_intervall, upper_intervall




def calc_median(data):
    data.sort()
    median = 0
    if len(data) % 2:
        i = int(len(data) / 2)
        median = float(data[i])
    else:
        i = len(data) / 2
        median = (float(data[i -1]) + float(data[i])) / 2
    return median


conn = sqlite3.connect('speed_1ms/benchmark.db')
c = conn.cursor()
c2 = conn.cursor()

c2.execute("""DELETE FROM SimpleMedian""")
c2.execute("""DELETE FROM SimpleMedianConvidenceIntervall""")
conn.commit()


for size in c.execute("""SELECT width, height FROM log GROUP BY width, height"""):
    logs = c2.execute("""SELECT *
                        FROM log 
                        WHERE width = ? and height = ? """, (size[0], size[1])).fetchall()
    start = logs[0][0]
    end = logs[len(logs)- 1][1]
    duations = []
    for log in logs:
        duations.append(log[2])

    durationsMedian = calc_median(duations)
    durationsMedianIntervall = calc_confidence_intervall(duations, durationsMedian)

    pss = c2.execute("""SELECT *
                        FROM ps
                        WHERE strftime(?) < strftime(ps.DATE||'/'||ps.TIME) and 
                        strftime(?) > strftime(ps.DATE||'/'||ps.TIME)""", (start, end)).fetchall()
    GPUs = []
    gpuSpeeds = []
    cpu0Speeds = []
    cpu1Speeds = []
    cpu2Speeds = []
    cpu3Speeds = []
    AOtherms = []
    CPUtherms = []
    GPUtherms = []
    PLLtherms = []
    PMICDies = []
    Tdiode_tegras = []
    Tboard_tegras = []
    thermalfan_est46s = []
    CPUs = []
    MEMs = []
    RSSs = []
    VSIZEs = []

    for ps in pss:
        GPUs.append(ps[3])
        gpuSpeeds.append(ps[4])
        cpu0Speeds.append(ps[5])
        cpu1Speeds.append(ps[6])
        cpu2Speeds.append(ps[7])
        cpu3Speeds.append(ps[8])
        AOtherms.append(ps[9])
        CPUtherms.append(ps[10])
        GPUtherms.append(ps[11])
        PLLtherms.append(ps[12])
        PMICDies.append(ps[13])
        Tdiode_tegras.append(ps[14])
        Tboard_tegras.append(ps[15])
        thermalfan_est46s.append(ps[16])
        CPUs.append(ps[18])
        MEMs.append(ps[19])
        RSSs.append(ps[20])
        VSIZEs.append(ps[21])


    GPUMedian = calc_median(GPUs)
    GPUMedianIntervall = calc_confidence_intervall(GPUs, GPUMedian)

    gpuSpeedMedian = calc_median(gpuSpeeds)
    gpuSpeedMedianIntervall = calc_confidence_intervall(gpuSpeeds, gpuSpeedMedian)

    cpu0SpeedMedian = calc_median(cpu0Speeds)
    cpu0SpeedMedianIntervall = calc_confidence_intervall(cpu0Speeds, cpu0SpeedMedian)

    cpu1SpeedMedian = calc_median(cpu1Speeds)
    cpu1SpeedMedianIntervall = calc_confidence_intervall(cpu1Speeds, cpu1SpeedMedian)

    cpu2SpeedMedian = calc_median(cpu2Speeds)
    cpu2SpeedMedianIntervall = calc_confidence_intervall(cpu2Speeds, cpu2SpeedMedian)

    cpu3SpeedMedian = calc_median(cpu3Speeds)
    cpu3SpeedMedianIntervall = calc_confidence_intervall(cpu3Speeds, cpu3SpeedMedian)

    AOthermMedian = calc_median(AOtherms)
    AOthermMedianIntervall = calc_confidence_intervall(AOtherms, AOthermMedian)

    CPUthermMedian = calc_median(CPUtherms)
    CPUthermMedianIntervall = calc_confidence_intervall(CPUtherms, CPUthermMedian)

    GPUthermMedian = calc_median(GPUtherms)
    GPUthermMedianIntervall = calc_confidence_intervall(GPUtherms, GPUthermMedian)

    PLLthermMedian = calc_median(PLLtherms)
    PLLthermMedianIntervall = calc_confidence_intervall(PLLtherms, PLLthermMedian)

    PMICDieMedian = calc_median(PMICDies)
    PMICDieMedianIntervall = calc_confidence_intervall(PMICDies, PMICDieMedian)

    Tdiode_tegraMedian = calc_median(Tdiode_tegras)
    Tdiode_tegraMedianIntervall = calc_confidence_intervall(Tdiode_tegras, Tdiode_tegraMedian)

    Tboard_tegraMedian = calc_median(Tboard_tegras)
    Tboard_tegraMedianIntervall = calc_confidence_intervall(Tboard_tegras, Tboard_tegraMedian)

    thermalfan_est46Median = calc_median(thermalfan_est46s)
    thermalfan_est46MedianIntervall = calc_confidence_intervall(thermalfan_est46s, thermalfan_est46Median)

    CPUMedian = calc_median(CPUs)
    CPUMedianIntervall = calc_confidence_intervall(CPUs, CPUMedian)

    MEMMedian = calc_median(MEMs)
    MEMMedianIntervall = calc_confidence_intervall(MEMs, MEMMedian)

    RSSMedian = calc_median(RSSs)
    RSSMedianIntervall = calc_confidence_intervall(RSSs, RSSMedian)

    VSIZEMedian = calc_median(VSIZEs)
    VSIZEMedianIntervall = calc_confidence_intervall(VSIZEs, VSIZEMedian)

    c2.execute('insert into SimpleMedian values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
               (str(start),
                str(end),
                str(size[0]),
                str(size[1]),
                str(durationsMedian),
                str(GPUMedian),
                str(gpuSpeedMedian),
                str(cpu0SpeedMedian),
                str(cpu1SpeedMedian),
                str(cpu2SpeedMedian),
                str(cpu3SpeedMedian),
                str(AOthermMedian),
                str(CPUthermMedian),
                str(GPUthermMedian),
                str(PLLthermMedian),
                str(PMICDieMedian),
                str(Tdiode_tegraMedian),
                str(Tboard_tegraMedian),
                str(thermalfan_est46Median),
                str(CPUMedian),
                str(MEMMedian),
                str(RSSMedian),
                str(VSIZEMedian))).fetchall()


    c2.execute('insert into SimpleMedianConvidenceIntervall values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
               (str(start),
                str(end),
                str(size[0]),
                str(size[1]),
                str(durationsMedianIntervall[0]),
                str(durationsMedianIntervall[1]),
                str(GPUMedianIntervall[0]),
                str(GPUMedianIntervall[1]),
                str(gpuSpeedMedianIntervall[0]),
                str(gpuSpeedMedianIntervall[1]),
                str(cpu0SpeedMedianIntervall[0]),
                str(cpu0SpeedMedianIntervall[1]),
                str(cpu1SpeedMedianIntervall[0]),
                str(cpu1SpeedMedianIntervall[1]),
                str(cpu2SpeedMedianIntervall[0]),
                str(cpu2SpeedMedianIntervall[1]),
                str(cpu3SpeedMedianIntervall[0]),
                str(cpu3SpeedMedianIntervall[1]),
                str(AOthermMedianIntervall[0]),
                str(AOthermMedianIntervall[1]),
                str(CPUthermMedianIntervall[0]),
                str(CPUthermMedianIntervall[1]),
                str(GPUthermMedianIntervall[0]),
                str(GPUthermMedianIntervall[1]),
                str(PLLthermMedianIntervall[0]),
                str(PLLthermMedianIntervall[1]),
                str(PMICDieMedianIntervall[0]),
                str(PMICDieMedianIntervall[1]),
                str(Tdiode_tegraMedianIntervall[0]),
                str(Tdiode_tegraMedianIntervall[1]),
                str(Tboard_tegraMedianIntervall[0]),
                str(Tboard_tegraMedianIntervall[1]),
                str(thermalfan_est46MedianIntervall[0]),
                str(thermalfan_est46MedianIntervall[1]),
                str(CPUMedianIntervall[0]),
                str(CPUMedianIntervall[1]),
                str(MEMMedianIntervall[0]),
                str(MEMMedianIntervall[1]),
                str(RSSMedianIntervall[0]),
                str(RSSMedianIntervall[1]),
                str(VSIZEMedianIntervall[0]),
                str(VSIZEMedianIntervall[1]))).fetchall()

conn.commit()
c2.close()
c.close()
conn.close()


# maxDuration = c2.execute("""SELECT MAX(duration)
#                             FROM merge 
#                             WHERE model = ? AND batchsize = ?""",
#                             (row[0], row[1],)).fetchone()[0]
# minDuration = c2.execute("""SELECT MIN(duration)
#                             FROM merge
#                             WHERE model = ? AND batchsize = ?""",
#                             (row[0], row[1],)).fetchone()[0]

# maxGpu = c2.execute("""SELECT MAX(gpu) FROM merge WHERE model = ? AND batchsize = ?""",
#                     (row[0], row[1],)).fetchone()[0]
# minGpu = c2.execute("""SELECT MIN(gpu) FROM merge WHERE model = ? AND batchsize = ?""",
#                     (row[0], row[1],)).fetchone()[0]

# maxCpu = c2.execute("""SELECT MAX(cpu) FROM merge WHERE model = ? AND batchsize = ?""",
#                     (row[0], row[1],)).fetchone()[0]
# minCpu = c2.execute("""SELECT MIN(cpu) FROM merge WHERE model = ? AND batchsize = ?""",
#                     (row[0], row[1],)).fetchone()[0]


# avgDuration = c2.execute("""SELECT AVG(duration)
#                             FROM (SELECT duration
#                                 FROM merge
#                                 WHERE
#                                     duration < ? AND
#                                     duration > ? AND
#                                     model = ? AND
#                                     batchsize = ?)""",
#                             (maxDuration, minDuration, row[0], row[1],)).fetchone()

# avgGpu = c2.execute("""SELECT AVG(gpu)
#                             FROM (SELECT gpu
#                                 FROM merge
#                                 WHERE
#                                     gpu < ? AND
#                                     gpu > ? AND
#                                     model = ? AND
#                                     batchsize = ?)""",
#                     (maxGpu, minGpu, row[0], row[1],)).fetchone()

# avgCpu = c2.execute("""SELECT AVG(cpu)
#                         FROM merge
#                         WHERE
#                             model = ? AND
#                             batchsize = ?""",
#                     (row[0], row[1],)).fetchone()

# avgMem = c2.execute("""SELECT AVG(mem)
#                     FROM merge
#                     WHERE
#                         model = ? AND
#                         batchsize = ?""",
#                     (row[0], row[1],)).fetchone()

# avgRss = c2.execute("""SELECT AVG(rss)
#                     FROM merge
#                     WHERE
#                         model = ? AND
#                         batchsize = ?""",
#                     (row[0], row[1],)).fetchone()

# avgVsize = c2.execute("""SELECT AVG(vsize)
#                         FROM merge
#                         WHERE
#                             model = ? AND
#                             batchsize = ?""",
#                         (row[0], row[1],)).fetchone()

# data = list(row) + list(avgDuration) + list(avgGpu) + list(avgCpu) + list(avgMem) + list(avgRss) + list(avgVsize)
# print data