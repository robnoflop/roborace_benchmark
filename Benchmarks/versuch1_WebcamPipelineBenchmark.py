#!/usr/bin/env python
import sys
import gi
import numpy as np, cv
import cv2
import csv
import gc
import time
import datetime
#from PIL import Image
gi.require_version('Gst', '1.0')
from gi.repository import Gst, GObject
import PipelineRunner

#PIPELINE = """nvcamerasrc ! video/x-raw(memory:NVMM), width=(int)1280, height=(int)720,format=(string)I420, framerate=(fraction)24/1 ! nvvidconv flip-method=2 ! video/x-raw, format=(string)BGRx ! #videoconvert ! video/x-raw, format=(string)BGR ! appsink name=app emit-signals=true sync=true auto-flush-bus=true"""


#http://www.equasys.de/standardresolution.html
resolutions = [[320,200], #1
		[320,240], #2
		[640,480],#3
                [800,600],#4
		#[854,480], error
		[1024,768],#5
                [1152,768],#6
		[1280,800],#7
		[1280,854],#8
                [1280,960],#9
		[1280,1024],#10
		[1400,1050],#11
                [1440,900],#12
		[1440,960],#13
		[1600,1200],#14
                [1680,1050],#15
		[1920,1200],#16
		[2048,1536],#17
                [2560,1600],#18
		[2560,2048]#19
		]

def main():
    with open('WebcamPipelineBenchmark.csv', 'wb') as csvfile:
        logger = csv.writer(csvfile, delimiter=',',
                            quotechar='|', quoting=csv.QUOTE_MINIMAL)
        logger.writerow(['starttime', 'endtime', 'duration', 'width', 'height', 'buffersize'])

	Gst.init(sys.argv)
        GObject.threads_init()

        for res in resolutions:
            PIPELINE = """nvcamerasrc num-buffers=1000 ! 
                        video/x-raw(memory:NVMM), width=(int)"""+str(res[0])+""", height=(int)"""+str(res[1])+""", format=(string)I420 ! 
                        nvvidconv ! 
                        video/x-raw, format=(string)BGRx ! 
                        videoconvert ! 
                        video/x-raw, format=(string)BGR ! 
                        appsink name=app emit-signals=true sync=false""" # drop=true max-buffers=2


            
            pipeline = Gst.parse_launch(PIPELINE)
            app = pipeline.get_by_name( 'app' )

	    #@profile
            def on_new_buffer( appsink ):
		
                
		start = time.time()

                sample = appsink.emit('pull-sample')
                buf = sample.get_buffer()
                caps = sample.get_caps()

                _, map = buf.map(Gst.MapFlags.READ)
                data = np.fromstring(map.data, dtype=np.uint8)
		buf.unmap(map) #prevent mem leak

                image = np.reshape(data, (caps.get_structure(0).get_value('height'), caps.get_structure(0).get_value('width'), 3))
		
                end = time.time()
                logger.writerow([datetime.datetime.fromtimestamp(start).strftime("%Y-%m-%d/%H:%M:%S.%f"), 
                                datetime.datetime.fromtimestamp(end).strftime("%Y-%m-%d/%H:%M:%S.%f"),
                                end - start,
                                res[0],
                                res[1],
				buf.get_size()])

		
                #print(caps.get_structure(0).get_value('format'))
                #print(caps.get_structure(0).get_value('height'))
                #print(caps.get_structure(0).get_value('width'))
                #print(buf.get_sizes())
		#print " "

		#sample = None
		#buf = None
		#caps = None		
		#map = None
		#data = None
		#image = None
		#x = None
		#appsink = None
		#gc.collect()
                #print image
                #vis2 = cv.CreateMat(caps.get_structure(0).get_value('height'), caps.get_structure(0).get_value('width'), cv.CV_32FC3)
                #vis0 = cv.fromarray(image)
                #cv2.imwrite('out.bmp', image)
                #result = Image.fromarray((image).astype(np.uint8))
                #result.save('out.bmp')
                return Gst.FlowReturn.OK

            app.connect('new-sample',on_new_buffer )
            r = PipelineRunner.PipelineRunner(pipeline)
            r.run()
	    r = None
	    pipline= None
	    app = None
	    gc.collect()
		


if __name__ == "__main__":
	main()


#http://elinux.org/Jetson/Thermal
#cat /sys/devices/virtual/thermal/thermal_zone*/type
#cat /sys/devices/virtual/thermal/thermal_zone*/temp


#http://elinux.org/Jetson/Performance
#cat /sys/kernel/debug/clock/gbus/rate

