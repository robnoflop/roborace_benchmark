import sqlite3


#mapping
# 0 = alexnet           1
# 1 = inception v1      2
# 2 = inception v2      3
# 3 = inception v3      4
# 5 = resnet v1 50      5
# 6 = resnet v1 101     6
# 7 = resnet v1 152     7
# 8 = resnet v1 200     8
# 9 = resnet v2 50      9
# 10 = resnet v2 101    10 
# 11 = resnet v2 152    11
# 12 = resnet v2 200    12
# 13 = vgg 16           13
# 14 = vgg 19           14

def print_tex_plot(data, caption):
    values = ""
    number = 1
    for d in data:
        values += d[1]
        values += "\t"
        values += str(number)
        values += "\t"
        values += str(float(d[1]) - float(d[2]))
        values += "\n"
        number += 1


    plot = """\\begin{figure}
    \\centering
    \\begin{tikzpicture}
    \\begin{axis}[xbar]
    \\addplot[
        every node near coord/.style={inner ysep=5pt},
        error bars/.cd,
            x dir=both,
            x explicit
    ] 
    table [x error=error] {
    x   y           error\n"""
    plot += values
    plot += """};
    \\end{axis}
    \\end{tikzpicture}
    \\caption{"""
    plot += caption
    plot += """}
    \\end{figure}
    \clearpage"""

    print(plot)

messung = 'normal_1ms'
#messung = 'normal_1ms_train'
#messung = 'normal_10ms'
#messung = 'normal_10ms_train_1h'
#messung = 'normal_10ms_train_20min'
#messung = 'normal_10ms_train_40min'
#messung = 'speed_1ms'
#messung = 'speed_1ms_train'
#messung = 'speed_10ms'
#messung = 'speed_10ms (repeat)'
#messung = 'speed_10ms_train'


conn = sqlite3.connect(messung + '/benchmark.db')
c = conn.cursor()



data = c.execute("""SELECT a.model, a.`AVG(duration)`, b.`median(durationUnder)`, b.`median(durationUpper)`
FROM SimpleAvrage a , SimpleAvrageConvidenceIntervall b
WHERE a.model = b.model""").fetchall()
print_tex_plot(data, "Network Test Avarage Duration (" + messung + ")")

data = c.execute("""SELECT a.model, a.`AVG(GPU)`, b.`median(GPUUnder)`, b.`median(GPUUpper)`
FROM SimpleAvrage a , SimpleAvrageConvidenceIntervall b
WHERE a.model = b.model""").fetchall()
print_tex_plot(data, "Network Test Avarage GPU (" + messung + ")")

data = c.execute("""SELECT a.model, a.`AVG(gpuSpeed)`, b.`median(gpuSpeedUnder)`, b.`median(gpuSpeedUpper)`
FROM SimpleAvrage a , SimpleAvrageConvidenceIntervall b
WHERE a.model = b.model""").fetchall()
print_tex_plot(data, "Network Test Avarage gpuSpeed (" + messung + ")")

data = c.execute("""SELECT a.model, a.`AVG(cpu0Speed)`, b.`median(cpu0SpeedUnder)`, b.`median(cpu0SpeedUpper)`
FROM SimpleAvrage a , SimpleAvrageConvidenceIntervall b
WHERE a.model = b.model""").fetchall()
print_tex_plot(data, "Network Test Avarage cpu0Speed (" + messung + ")")

data = c.execute("""SELECT a.model, a.`AVG(cpu1Speed)`, b.`median(cpu1SpeedUnder)`, b.`median(cpu1SpeedUpper)`
FROM SimpleAvrage a , SimpleAvrageConvidenceIntervall b
WHERE a.model = b.model""").fetchall()
print_tex_plot(data, "Network Test Avarage cpu1Speed (" + messung + ")")

data = c.execute("""SELECT a.model, a.`AVG(cpu2Speed)`, b.`median(cpu2SpeedUnder)`, b.`median(cpu2SpeedUpper)`
FROM SimpleAvrage a , SimpleAvrageConvidenceIntervall b
WHERE a.model = b.model""").fetchall()
print_tex_plot(data, "Network Test Avarage cpu2Speed (" + messung + ")")

data = c.execute("""SELECT a.model, a.`AVG(cpu3Speed)`, b.`median(cpu3SpeedUnder)`, b.`median(cpu3SpeedUpper)`
FROM SimpleAvrage a , SimpleAvrageConvidenceIntervall b
WHERE a.model = b.model""").fetchall()
print_tex_plot(data, "Network Test Avarage cpu3Speed (" + messung + ")")

# data = c.execute("""SELECT a.model, a.`AVG(``AO-therm``)`, b.`median(AO-thermUnder)`, b.`median(AO-thermUpper)`
# FROM SimpleAvrage a , SimpleAvrageConvidenceIntervall b
# WHERE a.model = b.model""").fetchall()
# print_tex_plot(data, "Network Test Avarage AO-therm (" + messung + ")")

data = c.execute("""SELECT a.model, a.`AVG(``CPU-therm``)`, b.`median(CPU-thermUnder)`, b.`median(CPU-thermUpper)`
FROM SimpleAvrage a , SimpleAvrageConvidenceIntervall b
WHERE a.model = b.model""").fetchall()
print_tex_plot(data, "Network Test Avarage CPU-therm (" + messung + ")")

data = c.execute("""SELECT a.model, a.`AVG(``GPU-therm``)`, b.`median(GPU-thermUnder)`, b.`median(GPU-thermUpper)`
FROM SimpleAvrage a , SimpleAvrageConvidenceIntervall b
WHERE a.model = b.model""").fetchall()
print_tex_plot(data, "Network Test Avarage GPU-therm (" + messung + ")")

# data = c.execute("""SELECT a.model, a.`AVG(``PLL-therm``)`, b.`median(PLL-thermUnder)`, b.`median(PLL-thermUpper)`
# FROM SimpleAvrage a , SimpleAvrageConvidenceIntervall b
# WHERE a.model = b.model""").fetchall()
# print_tex_plot(data, "Network Test Avarage Duration (" + messung + ")")

# data = c.execute("""SELECT a.model, a.`AVG(``PMIC-Die``)`, b.`median(PMIC-DieUnder)`, b.`median(PMIC-DieUpper)`
# FROM SimpleAvrage a , SimpleAvrageConvidenceIntervall b
# WHERE a.model = b.model""").fetchall()
# print_tex_plot(data, "Network Test Avarage PMIC-Die (" + messung + ")")

# data = c.execute("""SELECT a.model, a.`AVG(Tdiode_tegra)`, b.`median(Tdiode_tegraUnder)`, b.`median(Tdiode_tegraUpper)`
# FROM SimpleAvrage a , SimpleAvrageConvidenceIntervall b
# WHERE a.model = b.model""").fetchall()
# print_tex_plot(data, "Network Test Avarage Tdiode_tegra (" + messung + ")")

# data = c.execute("""SELECT a.model, a.`AVG(Tboard_tegra)`, b.`median(Tboard_tegraUnder)`, b.`median(Tboard_tegraUpper)`
# FROM SimpleAvrage a , SimpleAvrageConvidenceIntervall b
# WHERE a.model = b.model""").fetchall()
# print_tex_plot(data, "Network Test Avarage Tboard_tegra (" + messung + ")")

# data = c.execute("""SELECT a.model, a.`AVG(``thermal-fan_est.46``)`, b.`median(thermal-fan_est.46Under)`, b.`median(thermal-fan_est.46Upper)`
# FROM SimpleAvrage a , SimpleAvrageConvidenceIntervall b
# WHERE a.model = b.model""").fetchall()
# print_tex_plot(data, "Network Test Avarage thermal-fan_est.46 (" + messung + ")")

data = c.execute("""SELECT a.model, a.`AVG(CPU)`, b.`median(CPUUnder)`, b.`median(CPUUpper)`
FROM SimpleAvrage a , SimpleAvrageConvidenceIntervall b
WHERE a.model = b.model""").fetchall()
print_tex_plot(data, "Network Test Avarage CPU (" + messung + ")")

data = c.execute("""SELECT a.model, a.`AVG(MEM)`, b.`median(MEMUnder)`, b.`median(MEMUpper)`
FROM SimpleAvrage a , SimpleAvrageConvidenceIntervall b
WHERE a.model = b.model""").fetchall()
print_tex_plot(data, "Network Test Avarage MEM (" + messung + ")")

data = c.execute("""SELECT a.model, a.`AVG(RSS)`, b.`median(RSSUnder)`, b.`median(RSSUpper)`
FROM SimpleAvrage a , SimpleAvrageConvidenceIntervall b
WHERE a.model = b.model""").fetchall()
print_tex_plot(data, "Network Test Avarage RSS (" + messung + ")")

data = c.execute("""SELECT a.model, a.`AVG(VSIZE)`, b.`median(VSIZEUnder)`, b.`median(VSIZEUpper)`
FROM SimpleAvrage a , SimpleAvrageConvidenceIntervall b
WHERE a.model = b.model""").fetchall()
print_tex_plot(data, "Network Test Avarage VSIZE (" + messung + ")")




data = c.execute("""SELECT a.model, a.`median(duration)`, b.`median(durationUnder)`, b.`median(durationUpper)`
FROM SimpleMedian a , SimpleMedianConvidenceIntervall b
WHERE a.model = b.model""").fetchall()
print_tex_plot(data, "Network Test Median Duration (" + messung + ")")

# data = c.execute("""SELECT a.model, a.`median(GPU)`, b.`median(GPUUnder)`, b.`median(GPUUpper)`
# FROM SimpleMedian a , SimpleMedianConvidenceIntervall b
# WHERE a.model = b.model""").fetchall()
# print_tex_plot(data, "Network Test Median GPU (" + messung + ")")

# data = c.execute("""SELECT a.model, a.`median(gpuSpeed)`, b.`median(gpuSpeedUnder)`, b.`median(gpuSpeedUpper)`
# FROM SimpleMedian a , SimpleMedianConvidenceIntervall b
# WHERE a.model = b.model""").fetchall()
# print_tex_plot(data, "Network Test Median gpuSpeed (" + messung + ")")

# data = c.execute("""SELECT a.model, a.`median(cpu0Speed)`, b.`median(cpu0SpeedUnder)`, b.`median(cpu0SpeedUpper)`
# FROM SimpleMedian a , SimpleMedianConvidenceIntervall b
# WHERE a.model = b.model""").fetchall()
# print_tex_plot(data, "Network Test Median cpu0Speed (" + messung + ")")

# data = c.execute("""SELECT a.model, a.`median(cpu1Speed)`, b.`median(cpu1SpeedUnder)`, b.`median(cpu1SpeedUpper)`
# FROM SimpleMedian a , SimpleMedianConvidenceIntervall b
# WHERE a.model = b.model""").fetchall()
# print_tex_plot(data, "Network Test Median cpu1Speed (" + messung + ")")

# data = c.execute("""SELECT a.model, a.`median(cpu2Speed)`, b.`median(cpu2SpeedUnder)`, b.`median(cpu2SpeedUpper)`
# FROM SimpleMedian a , SimpleMedianConvidenceIntervall b
# WHERE a.model = b.model""").fetchall()
# print_tex_plot(data, "Network Test Median cpu2Speed (" + messung + ")")

# data = c.execute("""SELECT a.model, a.`median(cpu3Speed)`, b.`median(cpu3SpeedUnder)`, b.`median(cpu3SpeedUpper)`
# FROM SimpleMedian a , SimpleMedianConvidenceIntervall b
# WHERE a.model = b.model""").fetchall()
# print_tex_plot(data, "Network Test Median cpu3Speed (" + messung + ")")

# data = c.execute("""SELECT a.model, a.`median(AO-therm)`, b.`median(AO-thermUnder)`, b.`median(AO-thermUpper)`
# FROM SimpleMedian a , SimpleMedianConvidenceIntervall b
# WHERE a.model = b.model""").fetchall()
# print_tex_plot(data, "Network Test Median AO-therm (" + messung + ")")

# data = c.execute("""SELECT a.model, a.`median(CPU-therm)`, b.`median(CPU-thermUnder)`, b.`median(CPU-thermUpper)`
# FROM SimpleMedian a , SimpleMedianConvidenceIntervall b
# WHERE a.model = b.model""").fetchall()
# print_tex_plot(data, "Network Test Median CPU-therm (" + messung + ")")

# data = c.execute("""SELECT a.model, a.`median(GPU-therm)`, b.`median(GPU-thermUnder)`, b.`median(GPU-thermUpper)`
# FROM SimpleMedian a , SimpleMedianConvidenceIntervall b
# WHERE a.model = b.model""").fetchall()
# print_tex_plot(data, "Network Test Median GPU-therm (" + messung + ")")

# data = c.execute("""SELECT a.model, a.`median(PLL-therm)`, b.`median(PLL-thermUnder)`, b.`median(PLL-thermUpper)`
# FROM SimpleMedian a , SimpleMedianConvidenceIntervall b
# WHERE a.model = b.model""").fetchall()
# print_tex_plot(data, "Network Test Median Duration (" + messung + ")")

# data = c.execute("""SELECT a.model, a.`median(PMIC-Die)`, b.`median(PMIC-DieUnder)`, b.`median(PMIC-DieUpper)`
# FROM SimpleMedian a , SimpleMedianConvidenceIntervall b
# WHERE a.model = b.model""").fetchall()
# print_tex_plot(data, "Network Test Median PMIC-Die (" + messung + ")")

# data = c.execute("""SELECT a.model, a.`median(Tdiode_tegra)`, b.`median(Tdiode_tegraUnder)`, b.`median(Tdiode_tegraUpper)`
# FROM SimpleMedian a , SimpleMedianConvidenceIntervall b
# WHERE a.model = b.model""").fetchall()
# print_tex_plot(data, "Network Test Median Tdiode_tegra (" + messung + ")")

# data = c.execute("""SELECT a.model, a.`median(Tboard_tegra)`, b.`median(Tboard_tegraUnder)`, b.`median(Tboard_tegraUpper)`
# FROM SimpleMedian a , SimpleMedianConvidenceIntervall b
# WHERE a.model = b.model""").fetchall()
# print_tex_plot(data, "Network Test Median Tboard_tegra (" + messung + ")")

# data = c.execute("""SELECT a.model, a.`median(thermal-fan_est.46)`, b.`median(thermal-fan_est.46Under)`, b.`median(thermal-fan_est.46Upper)`
# FROM SimpleMedian a , SimpleMedianConvidenceIntervall b
# WHERE a.model = b.model""").fetchall()
# print_tex_plot(data, "Network Test Median thermal-fan_est.46 (" + messung + ")")

# data = c.execute("""SELECT a.model, a.`median(CPU)`, b.`median(CPUUnder)`, b.`median(CPUUpper)`
# FROM SimpleMedian a , SimpleMedianConvidenceIntervall b
# WHERE a.model = b.model""").fetchall()
# print_tex_plot(data, "Network Test Median CPU (" + messung + ")")

# data = c.execute("""SELECT a.model, a.`median(MEM)`, b.`median(MEMUnder)`, b.`median(MEMUpper)`
# FROM SimpleMedian a , SimpleMedianConvidenceIntervall b
# WHERE a.model = b.model""").fetchall()
# print_tex_plot(data, "Network Test Median MEM (" + messung + ")")

# data = c.execute("""SELECT a.model, a.`median(RSS)`, b.`median(RSSUnder)`, b.`median(RSSUpper)`
# FROM SimpleMedian a , SimpleMedianConvidenceIntervall b
# WHERE a.model = b.model""").fetchall()
# print_tex_plot(data, "Network Test Median RSS (" + messung + ")")

# data = c.execute("""SELECT a.model, a.`median(VSIZE)`, b.`median(VSIZEUnder)`, b.`median(VSIZEUpper)`
# FROM SimpleMedian a , SimpleMedianConvidenceIntervall b
# WHERE a.model = b.model""").fetchall()
# print_tex_plot(data, "Network Test Median VSIZE (" + messung + ")")




data = c.execute("""SELECT a.model, a.`median(duration)`, b.`median(durationUnder)`, b.`median(durationUpper)`
FROM SimpleMode a , SimpleModeConvidenceIntervall b
WHERE a.model = b.model""").fetchall()
print_tex_plot(data, "Network Test Mode Duration (" + messung + ")")

# data = c.execute("""SELECT a.model, a.`median(GPU)`, b.`median(GPUUnder)`, b.`median(GPUUpper)`
# FROM SimpleMode a , SimpleModeConvidenceIntervall b
# WHERE a.model = b.model""").fetchall()
# print_tex_plot(data, "Network Test Mode GPU (" + messung + ")")

# data = c.execute("""SELECT a.model, a.`median(gpuSpeed)`, b.`median(gpuSpeedUnder)`, b.`median(gpuSpeedUpper)`
# FROM SimpleMode a , SimpleModeConvidenceIntervall b
# WHERE a.model = b.model""").fetchall()
# print_tex_plot(data, "Network Test Mode gpuSpeed (" + messung + ")")

# data = c.execute("""SELECT a.model, a.`median(cpu0Speed)`, b.`median(cpu0SpeedUnder)`, b.`median(cpu0SpeedUpper)`
# FROM SimpleMode a , SimpleModeConvidenceIntervall b
# WHERE a.model = b.model""").fetchall()
# print_tex_plot(data, "Network Test Mode cpu0Speed (" + messung + ")")

# data = c.execute("""SELECT a.model, a.`median(cpu1Speed)`, b.`median(cpu1SpeedUnder)`, b.`median(cpu1SpeedUpper)`
# FROM SimpleMode a , SimpleModeConvidenceIntervall b
# WHERE a.model = b.model""").fetchall()
# print_tex_plot(data, "Network Test Mode cpu1Speed (" + messung + ")")

# data = c.execute("""SELECT a.model, a.`median(cpu2Speed)`, b.`median(cpu2SpeedUnder)`, b.`median(cpu2SpeedUpper)`
# FROM SimpleMode a , SimpleModeConvidenceIntervall b
# WHERE a.model = b.model""").fetchall()
# print_tex_plot(data, "Network Test Mode cpu2Speed (" + messung + ")")

# data = c.execute("""SELECT a.model, a.`median(cpu3Speed)`, b.`median(cpu3SpeedUnder)`, b.`median(cpu3SpeedUpper)`
# FROM SimpleMode a , SimpleModeConvidenceIntervall b
# WHERE a.model = b.model""").fetchall()
# print_tex_plot(data, "Network Test Mode cpu3Speed (" + messung + ")")

# data = c.execute("""SELECT a.model, a.`median(AO-therm)`, b.`median(AO-thermUnder)`, b.`median(AO-thermUpper)`
# FROM SimpleMode a , SimpleModeConvidenceIntervall b
# WHERE a.model = b.model""").fetchall()
# print_tex_plot(data, "Network Test Mode AO-therm (" + messung + ")")

# data = c.execute("""SELECT a.model, a.`median(CPU-therm)`, b.`median(CPU-thermUnder)`, b.`median(CPU-thermUpper)`
# FROM SimpleMode a , SimpleModeConvidenceIntervall b
# WHERE a.model = b.model""").fetchall()
# print_tex_plot(data, "Network Test Mode CPU-therm (" + messung + ")")

# data = c.execute("""SELECT a.model, a.`median(GPU-therm)`, b.`median(GPU-thermUnder)`, b.`median(GPU-thermUpper)`
# FROM SimpleMode a , SimpleModeConvidenceIntervall b
# WHERE a.model = b.model""").fetchall()
# print_tex_plot(data, "Network Test Mode GPU-therm (" + messung + ")")

# data = c.execute("""SELECT a.model, a.`median(PLL-therm)`, b.`median(PLL-thermUnder)`, b.`median(PLL-thermUpper)`
# FROM SimpleMode a , SimpleModeConvidenceIntervall b
# WHERE a.model = b.model""").fetchall()
# print_tex_plot(data, "Network Test Mode Duration (" + messung + ")")

# data = c.execute("""SELECT a.model, a.`median(PMIC-Die)`, b.`median(PMIC-DieUnder)`, b.`median(PMIC-DieUpper)`
# FROM SimpleMode a , SimpleModeConvidenceIntervall b
# WHERE a.model = b.model""").fetchall()
# print_tex_plot(data, "Network Test Mode PMIC-Die (" + messung + ")")

# data = c.execute("""SELECT a.model, a.`median(Tdiode_tegra)`, b.`median(Tdiode_tegraUnder)`, b.`median(Tdiode_tegraUpper)`
# FROM SimpleMode a , SimpleModeConvidenceIntervall b
# WHERE a.model = b.model""").fetchall()
# print_tex_plot(data, "Network Test Mode Tdiode_tegra (" + messung + ")")

# data = c.execute("""SELECT a.model, a.`median(Tboard_tegra)`, b.`median(Tboard_tegraUnder)`, b.`median(Tboard_tegraUpper)`
# FROM SimpleMode a , SimpleModeConvidenceIntervall b
# WHERE a.model = b.model""").fetchall()
# print_tex_plot(data, "Network Test Mode Tboard_tegra (" + messung + ")")

# data = c.execute("""SELECT a.model, a.`median(thermal-fan_est.46)`, b.`median(thermal-fan_est.46Under)`, b.`median(thermal-fan_est.46Upper)`
# FROM SimpleMode a , SimpleModeConvidenceIntervall b
# WHERE a.model = b.model""").fetchall()
# print_tex_plot(data, "Network Test Mode thermal-fan_est.46 (" + messung + ")")

# data = c.execute("""SELECT a.model, a.`median(CPU)`, b.`median(CPUUnder)`, b.`median(CPUUpper)`
# FROM SimpleMode a , SimpleModeConvidenceIntervall b
# WHERE a.model = b.model""").fetchall()
# print_tex_plot(data, "Network Test Mode CPU (" + messung + ")")

# data = c.execute("""SELECT a.model, a.`median(MEM)`, b.`median(MEMUnder)`, b.`median(MEMUpper)`
# FROM SimpleMode a , SimpleModeConvidenceIntervall b
# WHERE a.model = b.model""").fetchall()
# print_tex_plot(data, "Network Test Mode MEM (" + messung + ")")

# data = c.execute("""SELECT a.model, a.`median(RSS)`, b.`median(RSSUnder)`, b.`median(RSSUpper)`
# FROM SimpleMode a , SimpleModeConvidenceIntervall b
# WHERE a.model = b.model""").fetchall()
# print_tex_plot(data, "Network Test Mode RSS (" + messung + ")")

# data = c.execute("""SELECT a.model, a.`median(VSIZE)`, b.`median(VSIZEUnder)`, b.`median(VSIZEUpper)`
# FROM SimpleMode a , SimpleModeConvidenceIntervall b
# WHERE a.model = b.model""").fetchall()
# print_tex_plot(data, "Network Test Mode VSIZE (" + messung + ")")


# for model in c2.execute("""SELECT DISTINCT model
#                           FROM Gradients"""):

#     data = c.execute("""SELECT start, end, width, height, `gradient(GPU)`
#                         FROM Gradients
#                         WHERE width = ? AND height = ?""", (size[0], size[1])).fetchall()
#     print_tex_plot_gradient(data, "Webcam Test Gradient GPU (" + messung + ") (" + model[0] + ")")

#     data = c.execute("""SELECT start, end, width, height, `gradient(gpuSpeed)`
#                         FROM Gradients
#                         WHERE width = ? AND height = ?""", (size[0], size[1])).fetchall()
#     print_tex_plot_gradient(data, "Webcam Test Gradient gpuSpeed (" + messung + ") (" + model[0] + ")")

#     data = c.execute("""SELECT start, end, width, height, `gradient(cpu0Speed)`
#                         FROM Gradients
#                         WHERE width = ? AND height = ?""", (size[0], size[1])).fetchall()
#     print_tex_plot_gradient(data, "Webcam Test Gradient cpu0Speed (" + messung + ") (" + model[0] + ")")

#     data = c.execute("""SELECT start, end, width, height, `gradient(cpu1Speed)`
#                         FROM Gradients
#                         WHERE width = ? AND height = ?""", (size[0], size[1])).fetchall()
#     print_tex_plot_gradient(data, "Webcam Test Gradient cpu1Speed (" + messung + ") (" + model[0] + ")")

#     data = c.execute("""SELECT start, end, width, height, `gradient(cpu2Speed)`
#                         FROM Gradients
#                         WHERE width = ? AND height = ?""", (size[0], size[1])).fetchall()
#     print_tex_plot_gradient(data, "Webcam Test Gradient cpu2Speed (" + messung + ") (" + model[0] + ")")

#     data = c.execute("""SELECT start, end, width, height, `gradient(cpu3Speed)`
#                         FROM Gradients
#                         WHERE width = ? AND height = ?""", (size[0], size[1])).fetchall()
#     print_tex_plot_gradient(data, "Webcam Test Gradient cpu3Speed (" + messung + ") (" + model[0] + ")")

#     data = c.execute("""SELECT start, end, width, height, `gradient(AO-therm)`
#                         FROM Gradients
#                         WHERE width = ? AND height = ?""", (size[0], size[1])).fetchall()
#     print_tex_plot_gradient(data, "Webcam Test Gradient AO-therm (" + messung + ") (" + model[0] + ")")

#     data = c.execute("""SELECT start, end, width, height, `gradient(CPU-therm)`
#                         FROM Gradients
#                         WHERE width = ? AND height = ?""", (size[0], size[1])).fetchall()
#     print_tex_plot_gradient(data, "Webcam Test Gradient CPU-therm (" + messung + ") (" + model[0] + ")")

#     data = c.execute("""SELECT start, end, width, height, `gradient(GPU-therm)`
#                         FROM Gradients
#                         WHERE width = ? AND height = ?""", (size[0], size[1])).fetchall()
#     print_tex_plot_gradient(data, "Webcam Test Gradient GPU-therm (" + messung + ") (" + model[0] + ")")

#     data = c.execute("""SELECT start, end, width, height, `gradient(PLL-therm)`
#                         FROM Gradients
#                         WHERE width = ? AND height = ?""", (size[0], size[1])).fetchall()
#     print_tex_plot_gradient(data, "Webcam Test Gradient PLL-therm (" + messung + ") (" + model[0] + ")")

#     data = c.execute("""SELECT start, end, width, height, `gradient(PMIC-Die)`
#                         FROM Gradients
#                         WHERE width = ? AND height = ?""", (size[0], size[1])).fetchall()
#     print_tex_plot_gradient(data, "Webcam Test Gradient PMIC-Die (" + messung + ") (" + model[0] + ")")

#     data = c.execute("""SELECT start, end, width, height, `gradient(Tdiode_tegra)`
#                         FROM Gradients
#                         WHERE width = ? AND height = ?""", (size[0], size[1])).fetchall()
#     print_tex_plot_gradient(data, "Webcam Test Gradient Tdiode_tegra (" + messung + ") (" + model[0] + ")")

#     data = c.execute("""SELECT start, end, width, height, `gradient(Tboard_tegra)`
#                         FROM Gradients
#                         WHERE width = ? AND height = ?""", (size[0], size[1])).fetchall()
#     print_tex_plot_gradient(data, "Webcam Test Gradient Tboard_tegra (" + messung + ") (" + model[0] + ")")

#     data = c.execute("""SELECT start, end, width, height, `gradient(thermal-fan_est.46)`
#                         FROM Gradients
#                         WHERE width = ? AND height = ?""", (size[0], size[1])).fetchall()
#     print_tex_plot_gradient(data, "Webcam Test Gradient thermal-fan_est.46 (" + messung + ") (" + model[0] + ")")

#     data = c.execute("""SELECT start, end, width, height, `gradient(CPU)`
#                         FROM Gradients
#                         WHERE width = ? AND height = ?""", (size[0], size[1])).fetchall()
#     print_tex_plot_gradient(data, "Webcam Test Gradient CPU (" + messung + ") (" + model[0] + ")")

#     data = c.execute("""SELECT start, end, width, height, `gradient(MEM)`
#                         FROM Gradients
#                         WHERE width = ? AND height = ?""", (size[0], size[1])).fetchall()
#     print_tex_plot_gradient(data, "Webcam Test Gradient MEM (" + messung + ") (" + model[0] + ")")

#     data = c.execute("""SELECT start, end, width, height, `gradient(RSS)`
#                         FROM Gradients
#                         WHERE width = ? AND height = ?""", (size[0], size[1])).fetchall()
#     print_tex_plot_gradient(data, "Webcam Test Gradient RSS (" + messung + ") (" + model[0] + ")")

#     data = c.execute("""SELECT start, end, width, height, `gradient(VSIZE)`
#                         FROM Gradients
#                         WHERE width = ? AND height = ?""", (size[0], size[1])).fetchall()
#     print_tex_plot_gradient(data, "Webcam Test Gradient VSIZE (" + messung + ") (" + model[0] + ")")
