import sqlite3

conn = sqlite3.connect('normal_1ms_train/benchmark.db')
c = conn.cursor()
c2 = conn.cursor()

c2.execute("""DELETE FROM Gradients""")
conn.commit()


for model in c.execute("""SELECT DISTINCT model FROM log"""):
    logs = c2.execute("""SELECT *
                        FROM log 
                        WHERE model = ?""", model).fetchall()

    prevGPUgradient = 0.0
    prevgpuSpeedGradient = 0.0
    prevcpu0SpeedGradient = 0.0
    prevcpu1SpeedGradient = 0.0
    prevcpu2SpeedGradient = 0.0
    prevcpu3SpeedGradient = 0.0
    prevAOthermGradient = 0.0
    prevCPUthermGradient = 0.0 #
    prevGPUthermGradient = 0.0 #
    prevPLLthermGradient = 0.0
    prevPMICDieGradient = 0.0
    prevTdiode_tegraGradient = 0.0
    prevTboard_tegraGradient = 0.0
    prevthermalfan_est46Gradient = 0.0
    prevCPUGradient = 0.0
    prevMEMGradient = 0.0
    prevRSSGradient = 0.0
    prevVSIZEGradient = 0.0
    for log in logs:

        for ps in c2.execute("""SELECT *
                            FROM ps
                            WHERE strftime(?) < strftime(ps.DATE||'/'||ps.TIME) and 
                            strftime(?) > strftime(ps.DATE||'/'||ps.TIME)""", (log[0], log[1])):

            GPUgradient = float(ps[3]) - prevGPUgradient
            prevGPUgradient = float(ps[3])

            gpuSpeedGradient = float(ps[4]) - prevgpuSpeedGradient
            prevgpuSpeedGradient = float(ps[4])

            cpu0SpeedGradient = float(ps[5]) - prevcpu0SpeedGradient
            prevcpu0SpeedGradient = float(ps[5])

            cpu1SpeedGradient = float(ps[6]) - prevcpu1SpeedGradient
            prevcpu1SpeedGradient = float(ps[6])

            cpu2SpeedGradient = float(ps[7]) - prevcpu2SpeedGradient
            prevcpu2SpeedGradient = float(ps[7])

            cpu3SpeedGradient = float(ps[8]) - prevcpu3SpeedGradient
            prevcpu3SpeedGradient = float(ps[8])

            AOthermGradient = float(ps[9]) - prevAOthermGradient
            prevAOthermGradient = float(ps[9])

            CPUthermGradient = float(ps[10]) - prevCPUthermGradient
            prevCPUthermGradient = float(ps[10])

            GPUthermGradient = float(ps[11]) - prevGPUthermGradient
            prevGPUthermGradient = float(ps[11])

            PLLthermGradient = float(ps[12]) - prevPLLthermGradient
            prevPLLthermGradient = float(ps[12])

            PMICDieGradient = float(ps[13]) - prevPMICDieGradient
            prevPMICDieGradient = float(ps[13])

            Tdiode_tegraGradient = float(ps[14]) - prevTdiode_tegraGradient
            prevTdiode_tegraGradient = float(ps[14])

            Tboard_tegraGradient = float(ps[15]) - prevTboard_tegraGradient
            prevTboard_tegraGradient = float(ps[15])

            thermalfan_est46Gradient = float(ps[16]) - prevthermalfan_est46Gradient
            prevthermalfan_est46Gradient = float(ps[16])

            CPUGradient = float(ps[18]) - prevCPUGradient
            prevCPUGradient = float(ps[18])

            MEMGradient = float(ps[19]) - prevMEMGradient
            prevMEMGradient = float(ps[19])

            RSSGradient = float(ps[20]) - prevRSSGradient
            prevRSSGradient = float(ps[20])

            VSIZEGradient = float(ps[21]) - prevVSIZEGradient
            prevVSIZEGradient = float(ps[21])




            c2.execute('insert into Gradients values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
                       (str(log[0]),
                        str(log[1]),
                        str(model[0]),
                        #str(durationsMedian),
                        str(GPUgradient),
                        str(gpuSpeedGradient),
                        str(cpu0SpeedGradient),
                        str(cpu1SpeedGradient),
                        str(cpu2SpeedGradient),
                        str(cpu3SpeedGradient),
                        str(AOthermGradient),
                        str(CPUthermGradient),
                        str(GPUthermGradient),
                        str(PLLthermGradient),
                        str(PMICDieGradient),
                        str(Tdiode_tegraGradient),
                        str(Tboard_tegraGradient),
                        str(thermalfan_est46Gradient),
                        str(CPUGradient),
                        str(MEMGradient),
                        str(RSSGradient),
                        str(VSIZEGradient))).fetchall()

conn.commit()
c2.close()
c.close()
conn.close()


# maxDuration = c2.execute("""SELECT MAX(duration)
#                             FROM merge 
#                             WHERE model = ? AND batchsize = ?""",
#                             (row[0], row[1],)).fetchone()[0]
# minDuration = c2.execute("""SELECT MIN(duration)
#                             FROM merge
#                             WHERE model = ? AND batchsize = ?""",
#                             (row[0], row[1],)).fetchone()[0]

# maxGpu = c2.execute("""SELECT MAX(gpu) FROM merge WHERE model = ? AND batchsize = ?""",
#                     (row[0], row[1],)).fetchone()[0]
# minGpu = c2.execute("""SELECT MIN(gpu) FROM merge WHERE model = ? AND batchsize = ?""",
#                     (row[0], row[1],)).fetchone()[0]

# maxCpu = c2.execute("""SELECT MAX(cpu) FROM merge WHERE model = ? AND batchsize = ?""",
#                     (row[0], row[1],)).fetchone()[0]
# minCpu = c2.execute("""SELECT MIN(cpu) FROM merge WHERE model = ? AND batchsize = ?""",
#                     (row[0], row[1],)).fetchone()[0]


# avgDuration = c2.execute("""SELECT AVG(duration)
#                             FROM (SELECT duration
#                                 FROM merge
#                                 WHERE
#                                     duration < ? AND
#                                     duration > ? AND
#                                     model = ? AND
#                                     batchsize = ?)""",
#                             (maxDuration, minDuration, row[0], row[1],)).fetchone()

# avgGpu = c2.execute("""SELECT AVG(gpu)
#                             FROM (SELECT gpu
#                                 FROM merge
#                                 WHERE
#                                     gpu < ? AND
#                                     gpu > ? AND
#                                     model = ? AND
#                                     batchsize = ?)""",
#                     (maxGpu, minGpu, row[0], row[1],)).fetchone()

# avgCpu = c2.execute("""SELECT AVG(cpu)
#                         FROM merge
#                         WHERE
#                             model = ? AND
#                             batchsize = ?""",
#                     (row[0], row[1],)).fetchone()

# avgMem = c2.execute("""SELECT AVG(mem)
#                     FROM merge
#                     WHERE
#                         model = ? AND
#                         batchsize = ?""",
#                     (row[0], row[1],)).fetchone()

# avgRss = c2.execute("""SELECT AVG(rss)
#                     FROM merge
#                     WHERE
#                         model = ? AND
#                         batchsize = ?""",
#                     (row[0], row[1],)).fetchone()

# avgVsize = c2.execute("""SELECT AVG(vsize)
#                         FROM merge
#                         WHERE
#                             model = ? AND
#                             batchsize = ?""",
#                         (row[0], row[1],)).fetchone()

# data = list(row) + list(avgDuration) + list(avgGpu) + list(avgCpu) + list(avgMem) + list(avgRss) + list(avgVsize)
# print data