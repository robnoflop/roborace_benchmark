import sqlite3


def calc_mode(data):
    maxCount = 0
    mode = 0
    for a in data:
        count = data.count(a)
        if count > maxCount:
            mode = a
            maxCount = count

    return mode


conn = sqlite3.connect('normal_1ms/benchmark.db')
c = conn.cursor()
c2 = conn.cursor()

c2.execute("""DELETE FROM SimpleMode""")
conn.commit()


for model in c.execute("""SELECT DISTINCT model FROM log"""):
    logs = c2.execute("""SELECT *
                        FROM log 
                        WHERE model = ?""", model).fetchall()

    start = logs[0][0]
    end = logs[len(logs)- 1][1]
    duations = []
    for log in logs:
        duations.append(round(float(log[3]), 5))

    durationsMedian = calc_mode(duations)

    pss = c2.execute("""SELECT *
                        FROM ps
                        WHERE strftime(?) < strftime(ps.DATE||'/'||ps.TIME) and 
                        strftime(?) > strftime(ps.DATE||'/'||ps.TIME)""", (start, end)).fetchall()
    GPUs = []
    gpuSpeeds = []
    cpu0Speeds = []
    cpu1Speeds = []
    cpu2Speeds = []
    cpu3Speeds = []
    AOtherms = []
    CPUtherms = []
    GPUtherms = []
    PLLtherms = []
    PMICDies = []
    Tdiode_tegras = []
    Tboard_tegras = []
    thermalfan_est46s = []
    CPUs = []
    MEMs = []
    RSSs = []
    VSIZEs = []

    print model[0]

    for ps in pss:
        GPUs.append(round(float(ps[3]), 5))
        gpuSpeeds.append(round(float(ps[4]), 5))
        cpu0Speeds.append(round(float(ps[5]), 5))
        cpu1Speeds.append(round(float(ps[6]), 5))
        cpu2Speeds.append(round(float(ps[7]), 5))
        cpu3Speeds.append(round(float(ps[8]), 5))
        AOtherms.append(round(float(ps[9]), 5))
        CPUtherms.append(round(float(ps[10]), 5))
        GPUtherms.append(round(float(ps[11]), 5))
        PLLtherms.append(round(float(ps[12]), 5))
        PMICDies.append(round(float(ps[13]), 5))
        Tdiode_tegras.append(round(float(ps[14]), 5))
        Tboard_tegras.append(round(float(ps[15]), 5))
        thermalfan_est46s.append(round(float(ps[16]), 5))
        CPUs.append(round(float(ps[18]), 5))
        MEMs.append(round(float(ps[19]), 5))
        RSSs.append(round(float(ps[20]), 5))
        VSIZEs.append(round(float(ps[21]), 5))


    GPUMedian = calc_mode(GPUs)
    gpuSpeedMedian = calc_mode(gpuSpeeds)
    cpu0SpeedMedian = calc_mode(cpu0Speeds)
    cpu1SpeedMedian = calc_mode(cpu1Speeds)
    cpu2SpeedMedian = calc_mode(cpu2Speeds)
    cpu3SpeedMedian = calc_mode(cpu3Speeds)
    AOthermMedian = calc_mode(AOtherms)
    CPUthermMedian = calc_mode(CPUtherms)
    GPUthermMedian = calc_mode(GPUtherms)
    PLLthermMedian = calc_mode(PLLtherms)
    PMICDieMedian = calc_mode(PMICDies)
    Tdiode_tegraMedian = calc_mode(Tdiode_tegras)
    Tboard_tegraMedian = calc_mode(Tboard_tegras)
    thermalfan_est46Median = calc_mode(thermalfan_est46s)
    CPUMedian = calc_mode(CPUs)
    MEMMedian = calc_mode(MEMs)
    RSSMedian = calc_mode(RSSs)
    VSIZEMedian = calc_mode(VSIZEs)

    c2.execute('insert into SimpleMode values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
               (str(start),
                str(end),
                str(model[0]),
                str(durationsMedian),
                str(GPUMedian),
                str(gpuSpeedMedian),
                str(cpu0SpeedMedian),
                str(cpu1SpeedMedian),
                str(cpu2SpeedMedian),
                str(cpu3SpeedMedian),
                str(AOthermMedian),
                str(CPUthermMedian),
                str(GPUthermMedian),
                str(PLLthermMedian),
                str(PMICDieMedian),
                str(Tdiode_tegraMedian),
                str(Tboard_tegraMedian),
                str(thermalfan_est46Median),
                str(CPUMedian),
                str(MEMMedian),
                str(RSSMedian),
                str(VSIZEMedian))).fetchall()

conn.commit()
c2.close()
c.close()
conn.close()


# maxDuration = c2.execute("""SELECT MAX(duration)
#                             FROM merge 
#                             WHERE model = ? AND batchsize = ?""",
#                             (row[0], row[1],)).fetchone()[0]
# minDuration = c2.execute("""SELECT MIN(duration)
#                             FROM merge
#                             WHERE model = ? AND batchsize = ?""",
#                             (row[0], row[1],)).fetchone()[0]

# maxGpu = c2.execute("""SELECT MAX(gpu) FROM merge WHERE model = ? AND batchsize = ?""",
#                     (row[0], row[1],)).fetchone()[0]
# minGpu = c2.execute("""SELECT MIN(gpu) FROM merge WHERE model = ? AND batchsize = ?""",
#                     (row[0], row[1],)).fetchone()[0]

# maxCpu = c2.execute("""SELECT MAX(cpu) FROM merge WHERE model = ? AND batchsize = ?""",
#                     (row[0], row[1],)).fetchone()[0]
# minCpu = c2.execute("""SELECT MIN(cpu) FROM merge WHERE model = ? AND batchsize = ?""",
#                     (row[0], row[1],)).fetchone()[0]


# avgDuration = c2.execute("""SELECT AVG(duration)
#                             FROM (SELECT duration
#                                 FROM merge
#                                 WHERE
#                                     duration < ? AND
#                                     duration > ? AND
#                                     model = ? AND
#                                     batchsize = ?)""",
#                             (maxDuration, minDuration, row[0], row[1],)).fetchone()

# avgGpu = c2.execute("""SELECT AVG(gpu)
#                             FROM (SELECT gpu
#                                 FROM merge
#                                 WHERE
#                                     gpu < ? AND
#                                     gpu > ? AND
#                                     model = ? AND
#                                     batchsize = ?)""",
#                     (maxGpu, minGpu, row[0], row[1],)).fetchone()

# avgCpu = c2.execute("""SELECT AVG(cpu)
#                         FROM merge
#                         WHERE
#                             model = ? AND
#                             batchsize = ?""",
#                     (row[0], row[1],)).fetchone()

# avgMem = c2.execute("""SELECT AVG(mem)
#                     FROM merge
#                     WHERE
#                         model = ? AND
#                         batchsize = ?""",
#                     (row[0], row[1],)).fetchone()

# avgRss = c2.execute("""SELECT AVG(rss)
#                     FROM merge
#                     WHERE
#                         model = ? AND
#                         batchsize = ?""",
#                     (row[0], row[1],)).fetchone()

# avgVsize = c2.execute("""SELECT AVG(vsize)
#                         FROM merge
#                         WHERE
#                             model = ? AND
#                             batchsize = ?""",
#                         (row[0], row[1],)).fetchone()

# data = list(row) + list(avgDuration) + list(avgGpu) + list(avgCpu) + list(avgMem) + list(avgRss) + list(avgVsize)
# print data