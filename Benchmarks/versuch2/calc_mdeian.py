import sqlite3


def calc_median(data):
    data.sort()
    median = 0
    if len(data) % 2:
        i = int(len(data) / 2)
        median = float(data[i])
    else:
        i = len(data) / 2
        median = (float(data[i -1]) + float(data[i])) / 2
    return median


conn = sqlite3.connect('speed_10ms (repeat)/benchmark.db')
c = conn.cursor()
c2 = conn.cursor()

c2.execute("""DELETE FROM SimpleMedian""")
conn.commit()


for model in c.execute("""SELECT DISTINCT model FROM log"""):
    logs = c2.execute("""SELECT *
                        FROM log 
                        WHERE model = ?""", model).fetchall()
    start = logs[0][0]
    end = logs[len(logs)- 1][1]
    duations = []
    for log in logs:
        duations.append(log[3])

    durationsMedian = calc_median(duations)

    pss = c2.execute("""SELECT *
                        FROM ps
                        WHERE strftime(?) < strftime(ps.DATE||'/'||ps.TIME) and 
                        strftime(?) > strftime(ps.DATE||'/'||ps.TIME)""", (start, end)).fetchall()
    GPUs = []
    gpuSpeeds = []
    cpu0Speeds = []
    cpu1Speeds = []
    cpu2Speeds = []
    cpu3Speeds = []
    AOtherms = []
    CPUtherms = []
    GPUtherms = []
    PLLtherms = []
    PMICDies = []
    Tdiode_tegras = []
    Tboard_tegras = []
    thermalfan_est46s = []
    CPUs = []
    MEMs = []
    RSSs = []
    VSIZEs = []

    print model[0]

    for ps in pss:
        GPUs.append(ps[3])
        gpuSpeeds.append(ps[4])
        cpu0Speeds.append(ps[5])
        cpu1Speeds.append(ps[6])
        cpu2Speeds.append(ps[7])
        cpu3Speeds.append(ps[8])
        AOtherms.append(ps[9])
        CPUtherms.append(ps[10])
        GPUtherms.append(ps[11])
        PLLtherms.append(ps[12])
        PMICDies.append(ps[13])
        Tdiode_tegras.append(ps[14])
        Tboard_tegras.append(ps[15])
        thermalfan_est46s.append(ps[16])
        CPUs.append(ps[18])
        MEMs.append(ps[19])
        RSSs.append(ps[20])
        VSIZEs.append(ps[21])


    GPUMedian = calc_median(GPUs)
    gpuSpeedMedian = calc_median(gpuSpeeds)
    cpu0SpeedMedian = calc_median(cpu0Speeds)
    cpu1SpeedMedian = calc_median(cpu1Speeds)
    cpu2SpeedMedian = calc_median(cpu2Speeds)
    cpu3SpeedMedian = calc_median(cpu3Speeds)
    AOthermMedian = calc_median(AOtherms)
    CPUthermMedian = calc_median(CPUtherms)
    GPUthermMedian = calc_median(GPUtherms)
    PLLthermMedian = calc_median(PLLtherms)
    PMICDieMedian = calc_median(PMICDies)
    Tdiode_tegraMedian = calc_median(Tdiode_tegras)
    Tboard_tegraMedian = calc_median(Tboard_tegras)
    thermalfan_est46Median = calc_median(thermalfan_est46s)
    CPUMedian = calc_median(CPUs)
    MEMMedian = calc_median(MEMs)
    RSSMedian = calc_median(RSSs)
    VSIZEMedian = calc_median(VSIZEs)

    c2.execute('insert into SimpleMedian values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
               (str(start),
                str(end),
                str(model[0]),
                str(durationsMedian),
                str(GPUMedian),
                str(gpuSpeedMedian),
                str(cpu0SpeedMedian),
                str(cpu1SpeedMedian),
                str(cpu2SpeedMedian),
                str(cpu3SpeedMedian),
                str(AOthermMedian),
                str(CPUthermMedian),
                str(GPUthermMedian),
                str(PLLthermMedian),
                str(PMICDieMedian),
                str(Tdiode_tegraMedian),
                str(Tboard_tegraMedian),
                str(thermalfan_est46Median),
                str(CPUMedian),
                str(MEMMedian),
                str(RSSMedian),
                str(VSIZEMedian))).fetchall()


conn.commit()
c2.close()
c.close()
conn.close()


# maxDuration = c2.execute("""SELECT MAX(duration)
#                             FROM merge 
#                             WHERE model = ? AND batchsize = ?""",
#                             (row[0], row[1],)).fetchone()[0]
# minDuration = c2.execute("""SELECT MIN(duration)
#                             FROM merge
#                             WHERE model = ? AND batchsize = ?""",
#                             (row[0], row[1],)).fetchone()[0]

# maxGpu = c2.execute("""SELECT MAX(gpu) FROM merge WHERE model = ? AND batchsize = ?""",
#                     (row[0], row[1],)).fetchone()[0]
# minGpu = c2.execute("""SELECT MIN(gpu) FROM merge WHERE model = ? AND batchsize = ?""",
#                     (row[0], row[1],)).fetchone()[0]

# maxCpu = c2.execute("""SELECT MAX(cpu) FROM merge WHERE model = ? AND batchsize = ?""",
#                     (row[0], row[1],)).fetchone()[0]
# minCpu = c2.execute("""SELECT MIN(cpu) FROM merge WHERE model = ? AND batchsize = ?""",
#                     (row[0], row[1],)).fetchone()[0]


# avgDuration = c2.execute("""SELECT AVG(duration)
#                             FROM (SELECT duration
#                                 FROM merge
#                                 WHERE
#                                     duration < ? AND
#                                     duration > ? AND
#                                     model = ? AND
#                                     batchsize = ?)""",
#                             (maxDuration, minDuration, row[0], row[1],)).fetchone()

# avgGpu = c2.execute("""SELECT AVG(gpu)
#                             FROM (SELECT gpu
#                                 FROM merge
#                                 WHERE
#                                     gpu < ? AND
#                                     gpu > ? AND
#                                     model = ? AND
#                                     batchsize = ?)""",
#                     (maxGpu, minGpu, row[0], row[1],)).fetchone()

# avgCpu = c2.execute("""SELECT AVG(cpu)
#                         FROM merge
#                         WHERE
#                             model = ? AND
#                             batchsize = ?""",
#                     (row[0], row[1],)).fetchone()

# avgMem = c2.execute("""SELECT AVG(mem)
#                     FROM merge
#                     WHERE
#                         model = ? AND
#                         batchsize = ?""",
#                     (row[0], row[1],)).fetchone()

# avgRss = c2.execute("""SELECT AVG(rss)
#                     FROM merge
#                     WHERE
#                         model = ? AND
#                         batchsize = ?""",
#                     (row[0], row[1],)).fetchone()

# avgVsize = c2.execute("""SELECT AVG(vsize)
#                         FROM merge
#                         WHERE
#                             model = ? AND
#                             batchsize = ?""",
#                         (row[0], row[1],)).fetchone()

# data = list(row) + list(avgDuration) + list(avgGpu) + list(avgCpu) + list(avgMem) + list(avgRss) + list(avgVsize)
# print data